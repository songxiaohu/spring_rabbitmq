
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.tycmc.bulb.common.util.StringUtil;


import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

public class pushApsnDemo {
	/**
	 * 创建时间:2016-5-25 所在包:util 作者:王凯<br/>
	 * 方法说明：<br/>
	 * <br/>
	 * 参数： <br/>
	 * 返回值：是否操作成功
	 * Java服务端推送的时候提示：Received fatal alert: certificate_unknown
		其实原因很简单，导出p12的时候，要对整个证书导出，而不是对private key导出！！
	 */
	public void oo()  {

		// **APNS推送需要的证书、密码、和设备的Token ios 离线时不能收到所有推送**//*
		// java -jar apns-test-all.jar -f KOM_dev_java.p12 -p Ty19717610 -d
		// false -t
		// c6599a9b541ac2779322c4077ed53289201bcd7ada8128f48e21d2fa0b909f17
		
		 //String  p12Path = "D:/iphone/cty_dev_aps.p12"; 
		 String  p12Path = "D:/iospush/aa/cty_dl_dev.p12";  
	     String  password = "Ty19717610";  
	     String  pushToken = "5d572100488a743eae4793fe6e90f82230da373e37cfc001cd8de94cd744111f"; 
		// **设置参数，发送数据**//*
		ApnsService service = APNS.newService().withCert(p12Path, password).withSandboxDestination().build();// 发布用withProductionDestination()
		  //ApnsService service =APNS.newService().withCert(p12Path,password).withProductionDestination().build(); 
		String payload ="";
		String fltmsg="";
		// String pushToken =
		// "b4f19ffe0240f8685688d958399216e9a39c4617df67eaa8885cb2fe339f1e40";
			if(StringUtil.isValid(pushToken)){
				fltmsg ="你好!!";
				try {
					payload = APNS.newPayload().alertBody(fltmsg).badge(3).sound("default").customField("komicsmsg", "1").build();
					// 自己拼
					// payload =
					// "{\"aps\": {\"sound\": \"default\",\"alert\": \"测试\",\"badge\": 3,\"content-available\": 1},\"msgType\": 99}";
					System.out.println(payload);
					service.push(pushToken, payload);
					System.out.println("推送信息已发送！");
					// **反馈feedback,返回失效的设备列表**//*
					Map<String, Date> inactiveDevices = service.getInactiveDevices();
					Set<String> key = inactiveDevices.keySet();
					for (Iterator it = key.iterator(); it.hasNext();) {
						String s1 = (String) it.next();
						System.out.println("设备令牌 " + s1);// 键 设备令牌
						System.out.println("失效时间 " + inactiveDevices.get(s1));// 值 失效时间
					}

				} catch (Exception e) {
					System.out.println("出错了：" + e.getMessage());
				}
				String ss = System.currentTimeMillis() + "";
				System.out.println("时间：" + ss);
			}
	
	}
		 public static void main(String[] args) {  
			       
			      /**APNS推送需要的证书、密码、和设备的Token**/  
			 String  p12Path = "D:/iospush/aa/cty_dl_dev.p12";  
		     String  password = "Ty19717610";  
		     String  pushToken = "5d572100488a743eae4793fe6e90f82230da373e37cfc001cd8de94cd744111f"; 
			      try {  
			          /**设置参数，发送数据**/  
			          
			    	  //ApnsService service =APNS.newService().withCert(p12Path,password).withProductionDestination().build(); //withSandboxDestination() 
			    	ApnsService service =APNS.newService().withCert(p12Path,password).withSandboxDestination().build();
			    	  String payload = APNS.newPayload().alertBody("hello123").badge(3).sound("default").customField("komicsmsg", "1").build();
			    	 // String payload = APNS.newPayload().alertBody("hello,www.mbaike.net").badge(1).sound("default").build();  
			         service.push(pushToken, payload);  
			         System.out.println("推送信息已发送！");  
			      } catch (Exception e) {  
			          System.out.println("出错了："+e.getMessage());  
			      }  
			  } 

}
