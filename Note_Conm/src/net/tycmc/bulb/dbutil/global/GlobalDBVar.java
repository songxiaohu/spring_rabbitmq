package net.tycmc.bulb.dbutil.global;

import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;

import net.tycmc.bulb.note.core.Util.DateControl;


import com.tycmc.functions.util.DynamicReadConfig;
import com.tycmc.functions.util.MapGetter;

public class GlobalDBVar {
	public static Map<String,Object> dbMap = null;	//除月库的其他库数据库链接信息
	public static Map<String,Object> dbMonthMap = null;	//月库各数据库链接信息
	public static boolean isOk = false;
	
	/**
	 * 获取完整数据库名
	 * @param tableName	表名
	 * @param type	所在数据库（1：CTY_Vcl；2：CTY_Egn_A_主库；3：CTY_Egn_B_主库；4：月库）
	 * @param data type=4时，必填，表日期，格式为：2015-10-01 00:00:00
	 * @return SetDBType=1:openrowset('SQLOLEDB','192.168.30.101\GNSSv112';'sa';'tykj66TYKJ',DR9_CTY_MS2.dbo.V_VehicleInfo)
	 * 		SetDBType=2:LINK_CTY_Vcl.DR9_CTY_MS2.dbo.V_VehicleInfo
	 */
	public static String getDBFullName_Conn(String tableName,String type,String data){
		String SetDBType ="";
		try {
			SetDBType = DynamicReadConfig.getConfigProperty("config.properties", "SetDBType");
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(SetDBType.equals("1")){//1：openrowset；
			if(type.equals("4")){
				String month = DateControl.getFormatDate(data,11);
				return "openrowset("+MapGetter.getString(dbMonthMap, month)+tableName+")";
			}else{
				return "openrowset("+MapGetter.getString(dbMap, type)+tableName+")";
			}
		}else if(SetDBType.equals("2")){//2：数据库远程服务器链接
			if(type.equals("4")){
				String month = DateControl.getFormatDate(data,11);
				return MapGetter.getString(dbMonthMap, month)+tableName;
			}else{
				return MapGetter.getString(dbMap, type)+tableName;
			}
		}else {
			System.out.println("config.properties表中未设定SetDBType值，或该值在1和2之外！");
			return "error";
		}
	}
}
