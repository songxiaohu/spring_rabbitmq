package net.tycmc.bulb.dbutil.global;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import net.tycmc.bulb.note.core.Util.DateControl;
import net.tycmc.bulb.note.core.handler.MyBaseDAO;

import org.apache.commons.configuration.ConfigurationException;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.tycmc.functions.util.DynamicReadConfig;
import com.tycmc.functions.util.MapGetter;

/**
 * 定时更新数据库链接信息
 * 每5分钟执行更新一次
 * @author lixiaofan
 *
 */
public final class DBInfoService extends QuartzJobBean{
	private MyBaseDAO dao;
	public MyBaseDAO getDao() {
		return dao;
	}
	public void setDao(MyBaseDAO dao) {
		this.dao = dao;
	}
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		if(GlobalDBVar.isOk){
			System.out.println("************Tomcat启动初始化数据库连接信息成功，无需再初始化！************************************");
		}else{
			String sql = "select * from DataBaseInfo where getdate()>=DBInfo_Eff and getdate()<DBInfo_Exp";
			List<Map<String,Object>> databaselist=dao.getBaseDAO().execQuery(sql, new Object[]{});
			
			String sql_MS = "select top 1 * from DataBaseInfo where DBInfo_Type=6 and getdate()>=DBInfo_Eff and getdate()<DBInfo_Exp";
			Map<String,Object> dbMS = dao.getBaseDAO().execQueryOne(sql_MS, new Object[]{});
			
			if(databaselist.size()>0){
				GlobalDBVar.dbMap = new HashMap<String,Object>();
				GlobalDBVar.dbMonthMap = new HashMap<String,Object>();
				String SetDBType ="";
				try {
					SetDBType = DynamicReadConfig.getConfigProperty("config.properties", "SetDBType");
				} catch (ConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(SetDBType.equals("1")){//1：openrowset；
					openrowset(databaselist,dbMS);
				}else if(SetDBType.equals("2")){//2：数据库远程服务器链接
					databaselong(databaselist,dbMS);
				}else {
					System.out.println("config.properties表中未设定SetDBType值，或该值在1和2之外！");
				}
				GlobalDBVar.isOk = false;
				
			}else{
				System.out.println("*********ERROR******DataBaseInfoService**********:数据库链接信息初始化失败，DataBaseInfo表中没有值，请初始化数据并重新启动！！！！！！");
			}
			
			//释放内容
			databaselist=null;
		}
	}
	/**
	 * 数据库远程服务器链接
	 * @param databaselist
	 */
	public static void databaselong(List<Map<String,Object>> databaselist,Map<String,Object> dbMS){
		Map<String,Object> map;
		String type;	//数据库类型：1：CTY_Vcl；2：CTY_Egn_A_主库；3：CTY_Egn_B_主库；4：解码月库；
		String DBSLongName;		//远程服务器名称
		String dbname;		//数据库名
		String ip;
		String port;
		String dbServerName;
		String month;
		
		String DBInfo_IP_MS = MapGetter.getString(dbMS, "DBInfo_IP");
		String DBInfo_Port_MS = MapGetter.getString(dbMS, "DBInfo_Port");
		String DBInfo_DBServerName_MS = MapGetter.getString(dbMS, "DBInfo_DBServerName");
		
		for (int i = 0; i < databaselist.size(); i++) {
			map = databaselist.get(i);
			type = MapGetter.getString(map, "DBInfo_Type");
			DBSLongName = MapGetter.getString(map, "DBInfo_DBSLongName");
			dbname = MapGetter.getString(map, "DBInfo_DBName");
			dbname = MapGetter.getString(map, "DBInfo_DBName");
			ip = MapGetter.getString(dbMS, "DBInfo_IP");
			port = MapGetter.getString(dbMS, "DBInfo_Port");
			dbServerName = MapGetter.getString(dbMS, "DBInfo_DBServerName");
			month = MapGetter.getString(map, "DBInfo_DateMonth");
			
			if(ip.equals(DBInfo_IP_MS) && port.equals(DBInfo_Port_MS) && dbServerName.equals(DBInfo_DBServerName_MS)){//与业务库在同一个服务器中，不用远程链接的方式
				if(type.equals("1") || type.equals("2") || type.equals("3")){
					GlobalDBVar.dbMap.put(type, dbname+".dbo.");
				}else if(type.equals("4")){
					GlobalDBVar.dbMonthMap.put(DateControl.getFormatDate(month,11), dbname+".dbo.");
				}
			}else{//与业务库在同一个服务器中，不用远程链接的方式
				if(type.equals("1") || type.equals("2") || type.equals("3")){
					GlobalDBVar.dbMap.put(type, DBSLongName+"."+dbname+".dbo.");				
				}else if(type.equals("4")){
					GlobalDBVar.dbMonthMap.put(DateControl.getFormatDate(month,11), DBSLongName+"."+dbname+".dbo.");
				}
			}
		}
		//释放内容
		map = null;
		type = null;
		DBSLongName = null;
		dbname = null;
		ip = null;
		port = null;
		dbServerName = null;
		month = null;
		DBInfo_IP_MS = null;
		DBInfo_Port_MS = null;
		DBInfo_DBServerName_MS = null;
	}
	
	/**
	 * openrowset远程数据库链接方式
	 * @param databaselist
	 */
	public static void openrowset(List<Map<String,Object>> databaselist,Map<String,Object> dbMS){
		Map<String,Object> map;
		String type;	//数据库类型：1：CTY_Vcl；2：CTY_Egn_A_主库；3：CTY_Egn_B_主库；4：解码月库；5:统计任务备份数据库
		String dbservername;	//服务器名称
		String username;	//链接用户名
		String password;	//链接密码
		String dbname;		//数据库名
		String str;		//拼接字符串
		String ip;
		String port;
		String dbServerName;
		String month;
		
		String DBInfo_IP_MS = MapGetter.getString(dbMS, "DBInfo_IP");
		String DBInfo_Port_MS = MapGetter.getString(dbMS, "DBInfo_Port");
		String DBInfo_DBServerName_MS = MapGetter.getString(dbMS, "DBInfo_DBServerName");
		for (int i = 0; i < databaselist.size(); i++) {
			map = databaselist.get(i);
			type = MapGetter.getString(map, "DBInfo_Type");
			dbservername = MapGetter.getString(map, "DBInfo_DBServerName");
			username = MapGetter.getString(map, "DBInfo_UserName");
			password = MapGetter.getString(map, "DBInfo_Password");
			dbname = MapGetter.getString(map, "DBInfo_DBName");
			str = "'SQLOLEDB','"+dbservername+"';'"+username+"';'"+password+"',"+dbname+".dbo.";
			month = MapGetter.getString(map, "DBInfo_DateMonth");
			ip = MapGetter.getString(dbMS, "DBInfo_IP");
			port = MapGetter.getString(dbMS, "DBInfo_Port");
			dbServerName = MapGetter.getString(dbMS, "DBInfo_DBServerName");
			
			if(ip.equals(DBInfo_IP_MS) && port.equals(DBInfo_Port_MS) && dbServerName.equals(DBInfo_DBServerName_MS)){//与业务库在同一个服务器中，不用远程链接的方式
				if(type.equals("1") || type.equals("2") || type.equals("3")){
					GlobalDBVar.dbMap.put(type, dbname+".dbo.");
				}else if(type.equals("4")){
					GlobalDBVar.dbMonthMap.put(DateControl.getFormatDate(month,11), dbname+".dbo.");
				}
			}else{//与业务库在同一个服务器中，不用远程链接的方式
				if(type.equals("1") || type.equals("2") || type.equals("3")){
					GlobalDBVar.dbMap.put(type, str);				
				}else if(type.equals("4")){
					GlobalDBVar.dbMonthMap.put(DateControl.getFormatDate(month,11), str);
				}
			}
		}
		//释放内容
		map = null;
		type = null;
		dbservername = null;
		username = null;
		password = null;
		dbname = null;
		str = null;
		ip = null;
		port = null;
		dbServerName = null;
		month = null;
		DBInfo_IP_MS = null;
		DBInfo_Port_MS = null;
		DBInfo_DBServerName_MS = null;
	}
}
