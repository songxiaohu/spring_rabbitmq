﻿/****************************
文件名:DateControl.java
创建时间:2008-12-20 上午11:01:47
所在包:com.tools
作者:郑兴苗
说明:
****************************/
package net.tycmc.bulb.note.core.Util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateControl {
	
	/**
	 * 方法说明:输入日期与今天的日期比较
	 * <br/>String b_date 大日期
	 * <br/>String s_date 小日期
	 * <br/>返回日期文件名：20080611151112
	 */
	public static String getDate_NowDate(String date) {
		String str = "yes";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		try {
			//System.out.println("输入日期："+formatter.parse(date));
			if(date.length()==10){
				date = date+" 00:00:00";
			}
			long date_long = formatter.parse(date).getTime();
			Date now_date = new Date();
			//System.out.println("今天日期："+now_date);
			long now_date_long = now_date.getTime();
			if(date_long>now_date_long){
				str = "yes";
			}else{
				str = "no";
			}
		} catch (ParseException e) {
			System.out.println("类:DateControl 方法:getBDate_SDate 执行:输入日期与今天的日期比较 时发生:ParseException异常");
		}
		//System.out.println(str);
		return str;
	}
	
	/**
	 * 方法说明:生成日期文件名
	 * <br/>
	 * <br/>返回日期文件名：20080611151112
	 */
	public static String getDateFileName() {
		String FileName = null;
		SimpleDateFormat formatter = new SimpleDateFormat( "yyyyMMddHHmmssS" ); 
		Date date=new Date();
		FileName = formatter.format(date);	
		return FileName;
	}
	
	/**
	 * 方法说明:取得当前日期格式
	 * <br/>参数:i=0,结果 yyyy-MM-dd HH:mm:ss
	 * <br/>参数:i=1,结果 yyyy-MM-dd
	 * <br/>参数:i=2,结果 yyyy年MM月dd日 HH:mm:ss
	 * <br/>参数:i=3,结果 yyyy年MM月dd日
	 * <br/>参数:i=4,结果 yyyy-MM
	 * <br/>参数:i=5,结果 yyyyMMdd
	 * <br/>参数:i=6,结果 MM月dd日
	 * <br/>返回日期：
	 */
	public static String getDateTime(int i) {
		String nowdate = null;
		String Type = "yyyy-MM-dd HH:mm:ss";
		if(i==0){
			Type = "yyyy-MM-dd HH:mm:ss";
		}else if(i==1){
			Type = "yyyy-MM-dd";
		}else if(i==2){
			Type = "yyyy年MM月dd日 HH:mm:ss";
		}else if(i==3){
			Type = "yyyy年MM月dd日";
		}else if(i==4){
			Type = "yyyy-MM";
		}else if(i==5){
			Type = "yyyyMMdd";
		}else if(i==6){
			Type = "MM月dd日";
		}

		
		SimpleDateFormat formatter = new SimpleDateFormat(Type); 
		Date date=new Date();
		nowdate = formatter.format(date);	
		return nowdate;
	}
	
	/**
	 * 方法说明:格式化日期
	 * <br/>参数:date
	 * <br/>参数:i=0,结果 yyyy-MM-dd HH:mm:ss
	 * <br/>参数:i=1,结果 yyyy-MM-dd
	 * <br/>参数:i=2,结果 yyyy年MM月dd日 HH:mm:ss
	 * <br/>参数:i=3,结果 yyyy年MM月dd日
	 * <br/>参数:i=4,结果 yyyyMMdd
	 * <br/>返回日期：
	 */
	public static String getFormatDate(String date,int i) {
		String nowdate = null;
		String Type = "yyyy-MM-dd HH:mm:ss";
		if(i==0){
			Type = "yyyy-MM-dd HH:mm:ss";
		}else if(i==1){
			Type = "yyyy-MM-dd";
		}else if(i==2){
			Type = "yyyy年MM月dd日 HH:mm:ss";
		}else if(i==3){
			Type = "yyyy年MM月dd日";
		}else if(i==4){
			Type = "yyyyMMdd";
		}else if(i==5){
			Type = "MM月dd日";
		}else if(i==6){
			Type = "MM月dd号";
		}else if(i==7){
			Type = "dd日HH时mm分";
		}else if(i==8){
			Type="MM月dd日HH时mm分ss秒";
		}else if(i==9){
			Type="MM月dd日HH:mm:ss";
		}else if(i==10){
			Type = "M/d";
		}else if(i==11){
			Type = "yyyyMM";
		}
		if(date==null)return "";
		if(date.length()<=10){
			date += " 00:00:00";
		}
		try {
			Date date_gsh =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
			SimpleDateFormat formatter = new SimpleDateFormat(Type); 
			nowdate = formatter.format(date_gsh);	
			
		} catch (ParseException e) {
			System.out.println("类:DateControl 方法:getFormatDate 执行:转换日期格式 发生:ParseException异常");
		}
		
		return nowdate;
	}
	
	/**
	 *方法说明：获得本周的 周一 日期
	 *<br/>参数：Date date
	 *<br/>返回值：本周的 周一 日期
	 */
	public static String getMonday(Date date){
		   Calendar c = Calendar.getInstance();
		   c.setTime(date);
		   c.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
		   return new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
	}
	
	/**
	 *方法说明：获得本周的 周日 日期
	 *<br/>参数：Date date
	 *<br/>返回值：本周的 周日 日期
	 */
	public static String getSunday(Date date){
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY+6);
		return new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
	}
	
	/**
	 *方法说明：把18.5小时转换成18小时30分钟
	 *<br/>参数：小数时间
	 *<br/>返回值：十分秒的时间
	 */
	public static String Hms(float time){
		String _time = "";
		float getTime = time*60;
    	try{
    		DecimalFormat df = new DecimalFormat("00000");
    		String itime = df.format(getTime);
		
    		int ih = Integer.parseInt(itime)/60;
		
    		int is = Integer.parseInt(itime)%60;
		
    		_time = ih+"小时"+is+"分钟";
		
    		if(ih<1){
    			_time =is+"分钟";
    		}else if(is<1){
    			_time = ih+"小时";		
    		}
		}catch(Exception e){
	    	e.printStackTrace();
	    }
		return _time;
	}
	
	/**
	 *方法说明：把秒数转换成18小时30分钟
	 *<br/>参数：小数时间
	 *<br/>返回值：十分秒的时间
	 */
	public static String Hm(float time){
		String _time = "";
		//float getTime = time*60;
		try{
			DecimalFormat df = new DecimalFormat("00000");
			String itime = df.format(time);
			
			int ih = Integer.parseInt(itime)/3600;
			
			int is = Integer.parseInt(itime)%3600/60;
			
			_time = ih+"小时"+is+"分钟";
			
			if(ih<1){
				_time =is+"分钟";
			}else if(is<1){
				_time = ih+"小时";		
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return _time;
	}
	
	/**
	 *方法说明：计算两个时间差用秒数表示
	 *<br/>参数：两个时间
	 *<br/>返回值：十分秒的时间
	 */
	public static long getMiaoShu(String s_date,String e_date){
		long miaoshu = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		if(s_date.length()==10){
			s_date = s_date+" 00:00:00";
		}
		if(e_date.length()==10){
			e_date = e_date+" 23:59:59";
		}
		try {
			long s_date_long = formatter.parse(s_date).getTime();
			long e_date_long = formatter.parse(e_date).getTime();
			
			miaoshu = (e_date_long-s_date_long)/1000;
			
			//如果有余数
			if(miaoshu%2==1){
				miaoshu = miaoshu+1;
			}

		} catch (ParseException e) {
			System.out.println("类:DateControl 方法:getMiaoShu 执行:计算两个时间差用秒数表示 发生:ParseException异常");
		}
		return miaoshu;
	}
	
	/**
	 * 获取当前日期前N天的日期
	 */
	public static String getCurDateBeforeN(int n){
		
		Calendar ca = Calendar.getInstance();
		ca.add(Calendar.DATE, -n);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s_time = formatter.format(ca.getTime());
		
		return s_time;
	}
	
	/**
	 * 给指定日期增加天数
	 * @param date	格式为"yyyy-MM-dd"
	 * @param days	天数
	 * @return
	 */
	public static String getNewDate (String date,int days){
		String d="";
		SimpleDateFormat   format=new   SimpleDateFormat( "yyyy-MM-dd HH:mm:ss"); 
		Date dd;
		try {
			dd = format.parse(date);
			Calendar calendar=Calendar.getInstance(); 
			calendar.setTime(dd); 
			calendar.add(Calendar.DAY_OF_MONTH,days); 
			d= format.format(calendar.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return d;
	}
	
	/**
	 * 判断当前日期与指定日期间隔天数
	 * @param date 格式必须为：yyyy-MM-dd HH:mm:ss
	 * @return 当前日期大于指定日期返回正整数，当前日期小于指定日期返回负数
	 */
	public static int getGapDays(String date){
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Date date1;
		Date date2 = new Date();
		int days=0;
		try {
			date1 = format.parse(date);
			Calendar calendar1=Calendar.getInstance(); 
			calendar1.setTime(date1); 
			calendar1.set(java.util.Calendar.HOUR_OF_DAY, 0);   
			calendar1.set(java.util.Calendar.MINUTE, 0);   
			calendar1.set(java.util.Calendar.SECOND, 0);   

			Calendar calendar2=Calendar.getInstance(); 
			calendar2.setTime(date2); 
			calendar2.set(java.util.Calendar.HOUR_OF_DAY, 0);   
			calendar2.set(java.util.Calendar.MINUTE, 0);   
			calendar2.set(java.util.Calendar.SECOND, 0);   
			
			days = ((int) (calendar2.getTime().getTime() / 1000) - (int) (calendar1   
	                 .getTime().getTime() / 1000)) / 3600 / 24;   
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return days;
	}
	
	public static void main(String[] args) {
		System.out.println(getGapDays("2013-08-13 12:00:00"));
	}
	//返回一段时间内的自然月
		public static int[]  getMonthSql(String startTime,String endTime){
			String startMonth=startTime.substring(0, 7).replace("-", "");
			String endMonth=endTime.substring(0, 7).replace("-", "");
			int sm=Integer.parseInt(startMonth);
			int em=Integer.parseInt(endMonth);
			String sql="";
			int [] months=new int[em-sm+1];
			int j=0;
			for(int i=sm;i<=em;i++){
				months[j]=sm+j;
				j++;
			}
			return months;
		}
}