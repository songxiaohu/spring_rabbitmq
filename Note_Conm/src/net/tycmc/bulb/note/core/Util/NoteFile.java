/**
 * 将消息写入指定的文件
 * D:/NoteFile/{noteType}/{noteType}_yyyyMMdd
 * 
 **/
package net.tycmc.bulb.note.core.Util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import com.tycmc.functions.util.StringUtil;

public class NoteFile {
	private static Logger log = Logger.getLogger(NoteFile.class);
	private File outFile;
	private String noteType;
	private String tempMoth;
	
	public NoteFile(String noteType){
		this.noteType = noteType;
		if(null!=this.noteType&&this.noteType.length()>0){
			try{
				PropertiesConfiguration config = new PropertiesConfiguration("config.properties");
				String root = config.getString("noteFile");
				tempMoth = new SimpleDateFormat("yyyyMM").format(new Date());
				root = root.replaceAll("noteType", noteType).replaceAll("filename", this.noteType+"_"+tempMoth+".txt");
				this.outFile = new File(root);
				File tempRoot = new File(root.substring(0, root.lastIndexOf("/")));
				if(!tempRoot.exists()){
					tempRoot.mkdirs();
				}
				if(!this.outFile.exists()){
					this.outFile.createNewFile();
				}
			}catch(ConfigurationException e){
				
			} catch (IOException e) {
				
			}
		}
	}
	
	public boolean writeOutNote(String noteContent){
		boolean flag = false;
		if(this.outFile.exists()&&StringUtil.isValid(tempMoth)){
			BufferedWriter os = null;
			try {
				String nowmoth = new SimpleDateFormat("yyyyMM").format(new Date());
				if(!this.tempMoth.equals(nowmoth)){
					String tempStr = this.outFile.getName().substring(0, this.outFile.getName().indexOf("_"));
					this.outFile = new File(this.outFile.getParent()+"\\"+tempStr+"_"+nowmoth+".txt");
					if(!this.outFile.exists()){
						this.outFile.createNewFile();
					}
				}
				os =  new BufferedWriter(new FileWriter(this.outFile,true));
				os.write(noteContent);
				os.flush();
				os.close();
				flag = true;
			} catch (IOException e) {
				
			} finally{
				if(null!=os){
					try {
						os.flush();
						os.close();
					} catch (IOException e) {
						os = null;
					}
				}
			}
		}else{
			log.error(this.outFile+"不存在，"+tempMoth);
			log.error(new Date()+"接收到消息"+this.noteType+"  "+noteContent);
		}
		return flag;
	}
	public static void main(String[] args) {
		File file = new File("F:\\NoteFile\\FltNote\\FltNote_201503.txt");
		try {
			String nowmoth = new SimpleDateFormat("yyyyMM").format(new Date());
			String tempStr = file.getName().substring(0, file.getName().indexOf("_"));
			file = new File(file.getParent()+"\\"+tempStr+"_"+nowmoth+".txt");
			System.out.println(file.getAbsolutePath());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
