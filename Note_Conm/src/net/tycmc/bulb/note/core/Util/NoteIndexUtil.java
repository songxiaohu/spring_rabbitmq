package net.tycmc.bulb.note.core.Util;

import net.tycmc.bulb.note.core.handler.NoteTypeenum;

public class NoteIndexUtil {

	public String getNoteTableName(int noteType) {
		String tableName = "";
		switch(noteType){
		case 1:
			tableName = NoteTypeenum.noteFlt.getTableName();
			break;
		case 2:
			tableName = NoteTypeenum.noteFltRelease.getTableName();
			break;
		case 3:
			tableName = NoteTypeenum.noteFltServe.getTableName();
			break;
		case 4:
			tableName = NoteTypeenum.noteFltServeRe.getTableName();
			break;	
		case 5:
			tableName = NoteTypeenum.noteFltTwelve.getTableName();
			break;
		case 6:
			tableName = NoteTypeenum.noteAlarm.getTableName();
			break;
		case 7:
			tableName = NoteTypeenum.noteAlarmRelease.getTableName();
			break;
		case 8:
			tableName = NoteTypeenum.noteServerLine.getTableName();
			break;
		case 9:
			tableName = NoteTypeenum.noteAutoInstallVcl.getTableName();
			break;
		case 10:
			tableName = NoteTypeenum.noteGate.getTableName();
			break;
		case 11:
			tableName = NoteTypeenum.noteSensor.getTableName();
			break;
		case 12:
			tableName = NoteTypeenum.noteSensorRelease.getTableName();
			break;
		case 13:
			tableName = NoteTypeenum.noteIOFen.getTableName();
			break;
		case 14:
			tableName = NoteTypeenum.noteMaint.getTableName();
			break;
		case 15:
			tableName=NoteTypeenum.noteTankLevel.getTableName();
			break;
		default:
			break;
		}
		return tableName;
	}

	public String getNoteTableShort(int noteType) {
		String tableShort = "";
		switch(noteType){
		case 1:
			tableShort = "NF";
			break;
		case 2:
			tableShort = "NF";
			break;
		case 3:
			tableShort = "NFS";
			break;
		case 4:
			tableShort = "NFS";
			break;	
		case 5:
			tableShort = "NFT";
			break;
		case 6:
			tableShort = "NA";
			break;
		case 7:
			tableShort = "NA";
			break;
		case 8:
			tableShort = "NSD";
			break;
		case 9:
			tableShort = "NAIV";
			break;
		case 10:
			tableShort = "NGA";
			break;
		case 11:
			tableShort = "NSA";
			break;
		case 12:
			tableShort = "NSA";
			break;
		case 13:
			tableShort = "NIOFA";
			break;
		case 14:
			tableShort = "NMA";
			break;
		case 15:
			tableShort = "NTL";
			break;
		default:
			break;
		}
		return tableShort;
	}

	public String getNoteCheckTableName(int noteType) {
		String tableName = "";
		switch(noteType){
		case 1:
			tableName = "Send_Flt_Last";
			break;
		case 2:
			tableName = "Send_Flt_Last";
			break;
		case 3:
			tableName = "Send_Flt_Last";
			break;
		case 4:
			tableName = "Send_Flt_Last";
			break;	
		case 5:
			break;
		case 6:
			tableName = "Send_Alarm_Last";
			break;
		case 7:
			tableName = "Send_Alarm_Last";
			break;
		case 8:
			break;
		case 9:
			break;
		case 10:
			break;
		case 11:
			tableName = "Send_Alarm_Last";
			break;
		case 12:
			tableName = "Send_Alarm_Last";
			break;
		case 13:
			break;
		case 14:
			break;
		case 15:
			tableName = "Send_Alarm_Last";
			break;
		default:
			break;
		}
		return tableName;
	}

	public String getNoteCheckTableShort(int noteType) {
		String tableShort = "";
		switch(noteType){
		case 1:
			tableShort = "SFL";
			break;
		case 2:
			tableShort = "SFL";
			break;
		case 3:
			tableShort = "SFL";
			break;
		case 4:
			tableShort = "SFL";
			break;	
		case 5:
			break;
		case 6:
			tableShort = "SAL";
			break;
		case 7:
			tableShort = "SAL";
			break;
		case 8:
			break;
		case 9:
			break;
		case 10:
			break;
		case 11:
			tableShort = "SAL";
			break;
		case 12:
			tableShort = "SAL";
			break;
		case 13:
			break;
		case 14:
			break;
		case 15:
			tableShort = "SAL";
			break;
		default:
			break;
		}
		return tableShort;
	}
}