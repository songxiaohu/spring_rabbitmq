package net.tycmc.bulb.note.core.Util;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class JsonToMap {
	public JsonToMap(){
		
	}
	@SuppressWarnings("unchecked")
	public Map<String,Object> jsonToMapSingle(String json){
		Map<String, Object> reMap = null;
		if(null!=json&&json.length()>0){
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				reMap = objectMapper.readValue(json, Map.class);
			} catch (JsonParseException e) {
				
			} catch (JsonMappingException e) {
				
			} catch (IOException e) {
				
			}
		}
		return reMap;
	}

}
