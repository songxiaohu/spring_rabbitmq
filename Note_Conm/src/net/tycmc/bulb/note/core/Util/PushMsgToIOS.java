package net.tycmc.bulb.note.core.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.tycmc.bulb.common.util.MapGetter;
import net.tycmc.bulb.common.util.StringUtil;

import org.apache.commons.configuration.ConfigurationException;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.tycmc.functions.util.DynamicReadConfig;

public class PushMsgToIOS {
	/**
	 * 创建时间:2016-5-25 所在包:util 作者:王凯<br/>
	 * 方法说明：<br/>
	 * <br/>
	 * 参数： <br/>
	 * 返回值：是否操作成功
	 */
	public void sendMsg(List<Map<String, Object>> cloudPushlist,int type) {
		// **APNS推送需要的证书、密码、和设备的Token ios 离线时不能收到所有推送**//*
		// java -jar apns-test-all.jar -f KOM_dev_java.p12 -p Ty19717610 -d
		// false -t
		// c6599a9b541ac2779322c4077ed53289201bcd7ada8128f48e21d2fa0b909f17
		System.out.println("--------进入IOS苹果APNS推送服务，服务开始--------");
		String p12Path="";
		String password = "";
		try {
			if(type==5){
				p12Path = DynamicReadConfig.getConfigProperty("app.properties", "certificationPath");
				password= DynamicReadConfig.getConfigProperty("app.properties", "password");
			}else if(type==6){
				p12Path = DynamicReadConfig.getConfigProperty("app.properties", "certificationRoadPath");
				password = DynamicReadConfig.getConfigProperty("app.properties", "Roadpassword");
			}
				
		} catch (ConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}//"C:/iospush/2016/sumitomo_dev.p12";
		// String p12Path = "C:/iospush/aa/KOM_dis_java.p12";
		// String p12Path =
		// "C:/Users/zhoudongliang/Desktop/推送仅测试/iOS/推送/komics/小松证书/KOM_dis_java.p12";
		String pushToken = "";
		// **设置参数，发送数据**//*
		//ApnsService service = APNS.newService().withCert(p12Path, password).withProductionDestination().build();// 发布用withProductionDestination()
		/****************************小凡查资料更换service初始化方法******************/
		ApnsService service = APNS.newService().withCert(p12Path, password).withProductionDestination().asQueued().withNoErrorDetection().build();
		service.start();
		String payload ="";
		String fltmsg="";
		// String pushToken =
		// "b4f19ffe0240f8685688d958399216e9a39c4617df67eaa8885cb2fe339f1e40";
		System.out.println("cloudPushlist------------"+cloudPushlist.size());
		for (Map<String, Object> map : cloudPushlist) {
			pushToken = MapGetter.getString(map, "token");
			System.out.println("pushToken--------------------"+pushToken);
			if(StringUtil.isValid(pushToken)){
				fltmsg =  MapGetter.getString(map, "vehiclenumber")+"故障:"+ MapGetter.getString(map, "fltcode")+","+ MapGetter.getString(map, "fltdetialmsg")+","+MapGetter.getString(map, "risk");
				System.out.println("fltmsg---------------------"+fltmsg);
				try {
					payload = APNS.newPayload().alertBody(fltmsg).badge(3).sound("default").customField("komicsmsg", "1").build();
					// 自己拼
					// payload =
					// "{\"aps\": {\"sound\": \"default\",\"alert\": \"测试\",\"badge\": 3,\"content-available\": 1},\"msgType\": 99}";
					service.push(pushToken, payload);
					System.out.println("推送信息已发送！token:"+pushToken);
					service.stop();

				} catch (Exception e) {
					System.out.println("出错了：" + e.getMessage());
				}
				String ss =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
				System.out.println("时间：" + ss);
			}
		}
		System.out.println("------------IOS苹果APNS推送服务，服务结束-------------");
	}
}
