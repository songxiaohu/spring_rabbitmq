package net.tycmc.bulb.note.core.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateList {
	
	@SuppressWarnings("unchecked")
	public static ArrayList iterateTime(final String startTime, final String endTime){
		String year = "";
        String month = "";
        String day = "";
        ArrayList arr =new ArrayList();
		
		Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        int[] date = parseTime(startTime);
        start.set(date[0], date[1], date[2]);
        date = parseTime(endTime);
        end.set(date[0], date[1], date[2]);
        while(start.before(end)){
        	year = start.get(Calendar.YEAR)+"";
        	month = start.get(Calendar.MONTH)+"";
        	if(Integer.parseInt(month) == 0){
        		month = "12";
        	}
        	if(Integer.parseInt(month) < 10){
        		month = "0"+month;
        	}
        	
        	day = start.get(Calendar.DATE)+"";
        	if(Integer.parseInt(day) < 10){
        		day = "0"+day;
        	}
        	arr.add(year+"-"+month+"-"+day);
        	
        	if(month.equals("1")||month.equals("3")||month.equals("5")||month.equals("7")
        			||month.equals("8")||month.equals("10")||month.equals("12")){
        		if(!day.equals("31")){
        			 start.add(Calendar.DATE, 1);
        		}
        	}
        	if(month.equals("")||month.equals("3")||month.equals("5")||month.equals("7")
        			||month.equals("8")||month.equals("10")||month.equals("12")){
        		if(!day.equals("31")){
        			 start.add(Calendar.DATE, 1);
        		}
        	}
           
        }
        return arr;
    }

    //format : YYYY-MM-DD
    private static int[] parseTime(final String timeString){
        final int [] ret = new int[3];
        int index = 0;
        for(final String field : timeString.split("-")){
            ret[index] = Integer.parseInt(field);
            index++;
        }
        return ret;
    }

    //日期+天数
    public static String addDay(String dateStr, int days) throws Exception {
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = new GregorianCalendar();
		Date trialTime = DateList.strtoDate(dateStr);
		calendar.setTime(trialTime);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return  myFormatter.format(calendar.getTime());
	}
    
    //字符串转日期
    public static Date strtoDate(String dateStr) throws Exception {
		Date date = null;
		 		try {
		 	SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
			date = myFormatter.parse(dateStr);
		} catch (Exception e) {
			//Logger.warn("把“" + dateStr + "”从字符串转成Date型失败！");
		}
		return date;
	} 
    
    //日期转字符串
    public static String dateToString(Date d, String fmt) {
		try {
			SimpleDateFormat dfm = new SimpleDateFormat(fmt);
			return dfm.format(d);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return d.toString();
	}
    
    //得到下个月日期
    public static String getNextMonth(Date date) {
    	String   s = "";
    	Calendar c = Calendar.getInstance();
    	try {
			GregorianCalendar   mortgage   =   new   GregorianCalendar( c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));   
			mortgage.add(Calendar.MONTH,1);   
			Date   d   =   mortgage.getTime();   
			DateFormat   df   =   DateFormat.getDateInstance();   
			s   =   df.format(d);   
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return s;
	}
    
    /**
	 * 方法说明:格式化日期
	 * <br/>参数:date
	 * <br/>参数:i=0,结果 yyyy-MM-dd HH:mm:ss
	 * <br/>参数:i=1,结果 yyyy-MM-dd
	 * <br/>参数:i=2,结果 yyyy年MM月dd日 HH:mm:ss
	 * <br/>参数:i=3,结果 yyyy年MM月dd日
	 * <br/>参数:i=4,结果 yyyyMMdd
	 * <br/>参数:i=5,结果MM月dd日
	 * <br/>参数:i=6,结果MM月dd日HH时mm分
	 * <br/>参数:i=7,结果MM月dd日HH时
	 * <br/>参数:i=8,结果dd日HH:mm
	 * <br/>参数:i=9,结果dd日HH时
	 * <br/>参数:i=10,结果dd日HH时mm分
	 * <br/>参数:i=11,结果MM月dd日 HH:mm
	 * <br/>返回日期：
	 */
	public static String getFormatDate(String date,int i) {
		String nowdate = null;
		String Type = "yyyy-MM-dd HH:mm:ss";
		String pareType = "yyyy-MM-dd HH:mm:ss";
		if(i==0){
			Type = "yyyy-MM-dd HH:mm:ss";
			pareType = "yyyy-MM-dd HH:mm:ss";
		}else if(i==1){
			Type = "yyyy-MM-dd";
			pareType = "yyyy-MM-dd";
		}else if(i==2){
			Type = "yyyy年MM月dd日 HH:mm:ss";
			pareType = "yyyy-MM-dd HH:mm:ss";
		}else if(i==3){
			Type = "yyyy年MM月dd日";
			pareType = "yyyy-MM-dd";
		}else if(i==4){
			Type = "yyyyMMdd";
			pareType = "yyyy-MM-dd";
		}else if(i==5){
			Type = "MM月dd日";
			pareType = "yyyy-MM-dd";
		}else if(i==6){
			Type = "MM月dd日HH时mm分";
			pareType = "yyyy-MM-dd HH:mm";
		}else if(i==7){
			Type = "MM月dd日HH时";//2013.03.29 haoyang 添加
			pareType = "yyyy-MM-dd HH";
		}else if(i==8){
			Type = "dd日HH:mm";
			pareType = "yyyy-MM-dd HH:mm";
		}else if(i==9){
			Type = "dd日HH时";
			pareType = "yyyy-MM-dd HH:mm";
		}else if(i==10){
			Type = "dd日HH时mm分";
			pareType = "yyyy-MM-dd HH:mm";
		}else if(i==11){
			Type = "MM月dd日 HH:mm";
			pareType = "yyyy-MM-dd HH:mm";
		}
		
		if(date==null)return "";
		try {
			Date date_gsh =  new SimpleDateFormat(pareType).parse(date);
			SimpleDateFormat formatter = new SimpleDateFormat(Type); 
			nowdate = formatter.format(date_gsh);	
			
		} catch (ParseException e) {
			System.out.println("类:DateControl 方法:getFormatDate 执行:转换日期格式 发生:ParseException异常");
		}
		
		return nowdate;
	}
}
