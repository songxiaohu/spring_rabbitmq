package net.tycmc.bulb.note.core.thread;

import java.util.Map;

import net.tycmc.bulb.common.util.MapGetter;
import net.tycmc.bulb.note.core.handler.SMSConfigenum;
import net.tycmc.bulb.note.core.handler.SMSTypeenum;
import net.tycmc.bulb.note.core.handler.DAO.APPSendDAO;
import net.tycmc.bulb.note.core.handler.DAO.SMSSendDAO;
import net.tycmc.bulb.note.core.handler.DAO.VclPlatDefinedDAO;

import com.tycmc.functions.MapFunction;
import com.tycmc.functions.util.StringUtil;


public class FltAlarmRunner implements Runnable {
	private Map<String,Object> param;
	public FltAlarmRunner(Map<String,Object> map){
		this.param = map;
	}

	@Override
	public void run() {
 		if(null!=param && checkFltAlarmNote()){
			String VehicleID = this.param.get("VehicleID").toString();
			/*String MsgFltLo = this.param.remove("MsgFltLo").toString();
			String MsgFltLa = this.param.remove("MsgFltLa").toString();*/
			String CT300=this.param.remove("CT300").toString();
			boolean sendNote =false;//IEMS是否发送短信
			boolean sendAPP = false;//管理版
			boolean sendAPPSP = false;//支持版
			boolean sendAPPJM = false;//工程机械
			boolean GcicsendNote = false;//工程机械是否发送短息
			boolean resultSendNote=false;//是否发送短信
			boolean isGcic=false;//是否发送短信       
			boolean sendFltAlarmAPPJMSP = false;//非道路是否推送
			boolean sendFltAlarmNoRoadAPPIOS= false;//非道路ios服务版是否推送
			boolean sendFltAlarmRoadAPPIOS=false;//道路车队版ios是否推送
			String Sys_Gcic=this.param.remove("Sys_Gcic").toString();
			String GCICName=this.param.remove("GCICNAME").toString();
			/*//解析位置------开始
			String position = "";
			try {
				position = MapFunction.getPositionReviseNoReplaceDescription(MsgFltLo, MsgFltLa);
			} catch (Exception e) {
				
			}*/
			this.param.put("Position","");
			//解析位置------结束	
			VclPlatDefinedDAO platDAO = new VclPlatDefinedDAO();
			//根据spn、fmi查询故障的详细信息
			Map<String,Object> fltMap = platDAO.queryFltInfoOnPlatDefined(this.param);
 			this.param.put("MsgFltCode",MapGetter.getString(fltMap,"MsgFltCode"));
 			this.param.put("FltColorInfo",MapGetter.getString(fltMap,"FltColorInfo"));
 			this.param.put("FltContentAbbr",MapGetter.getString(fltMap,"FltContentAbbr"));
 			this.param.put("FltContent",MapGetter.getString(fltMap,"FltContent"));
 			this.param.put("FltRisk",MapGetter.getString(fltMap,"FltRisk"));
 			this.param.put("FltCode",MapGetter.getString(fltMap,"FltCode"));
 			this.param.put("FltSPN",MapGetter.getString(fltMap,"FltSPN"));
 			this.param.put("FltFMI",MapGetter.getString(fltMap,"FltFMI"));
 			this.param.put("FltLevel",MapGetter.getString(fltMap,"FltLevel"));
 			this.param.put("FltContentAbbr_EN",MapGetter.getString(fltMap,"FltContentAbbr_EN"));
 			this.param.put("FltContent_EN",MapGetter.getString(fltMap,"FltContent_EN"));
 			this.param.put("FltRisk_EN",MapGetter.getString(fltMap,"FltRisk_EN"));
 			Map<String,Object>mapvcl=platDAO.queryVclSysTemId(VehicleID);
 			String dateBaseName="";
 			if(Sys_Gcic.equals(MapGetter.getString(mapvcl,"Vcl_SysI_ID"))){
 			isGcic=true;
 			dateBaseName=GCICName;
 			 sendAPPJM = this.param.get("sendAPPJM").equals("true")?true:false;
 			 GcicsendNote = this.param.get("GcicsendNote").equals("true")?true:false;
 			sendFltAlarmAPPJMSP = this.param.get("sendFltAlarmAPPJMSP").equals("true")?true:false;
 			sendFltAlarmNoRoadAPPIOS = this.param.get("sendFltAlarmNoRoadAPPIOS").equals("true")?true:false;
 			}else{
			 sendNote = this.param.get("sendNote").equals("true")?true:false;
			 sendAPP = this.param.get("sendAPP").equals("true")?true:false;
			 sendAPPSP = this.param.get("sendAPPSP").equals("true")?true:false;
			 sendFltAlarmRoadAPPIOS = this.param.get("sendFltAlarmRoadAPPIOS").equals("true")?true:false;
 			}
 			if(GcicsendNote||sendNote){
 				resultSendNote=true;
 			}
			//根据设备ID查询设备信息
			Map<String,Object> tempVcl =  platDAO.queryVclInfo(VehicleID,dateBaseName);
			
			if(null != tempVcl && !tempVcl.isEmpty()){
				this.param.put("VehicleNumber", MapGetter.getString(tempVcl,"VehicleNumber"));
				this.param.put("DBCode", MapGetter.getString(tempVcl,"DBCode"));
				this.param.put("ESNNumber", MapGetter.getString(tempVcl,"ESNNumber"));
				this.param.put("VehicleContact", MapGetter.getString(tempVcl,"VehicleContact"));
				this.param.put("VehicleChargedMobile", MapGetter.getString(tempVcl,"VehicleChargedMobile"));
				this.param.put("OEMName", MapGetter.getString(tempVcl,"OEMName"));
				this.param.put("OEMTel", MapGetter.getString(tempVcl,"OEMTel"));
				this.param.put("Tmnl",MapGetter.getString(tempVcl,"Tmnl_SoftEdition"));
				//线性故障时为true
				boolean alarm = this.param.get("MsgFltType").toString().equals("0")?true:false;
				
				//发送短信
				if(resultSendNote){
					
					//1：故障、2：报警、3：传感器报警、4门控报警、5：进出围栏提示、6：服务期报警、7：自动安装、8：保养提示
					this.param.put("smsType",SMSTypeenum.smsFlt.getSmsType());
					
					this.param.put("noteState", alarm?"1":"0");
					
					//统一配置短信发送时间表的ID值
					if(alarm){
						this.param.put("SMSCfgType", SMSConfigenum.SMSCfgFlt.getName());
					}else{
						this.param.put("SMSCfgType", SMSConfigenum.SMSCfgFltRels.getName());
					}
					
					SMSSendDAO sms = new SMSSendDAO(isGcic);
					sms.sendSMS(this.param);
				}
				//推送app	1：管理版；2：支持板 3：工程机械版
				if(alarm){
					System.out.println("isGCIC-----"+isGcic);
					APPSendDAO app = new APPSendDAO(isGcic);
					if(sendAPP)
						app.sendAPP(this.param,1);
					if(sendAPPSP)
						app.sendAPP(this.param,2);
					if(sendAPPJM)
						app.sendAPP(this.param,3);
					if(sendFltAlarmAPPJMSP)
						app.sendAPP(this.param,4);
					//将IOS推送
					/*if(sendFltAlarmNoRoadAPPIOS)
						app.sendAPP(this.param,5);
					if(sendFltAlarmRoadAPPIOS)
						app.sendAPP(this.param,6);*/
				}
			}
		}
	}

	private boolean checkFltAlarmNote() {
		boolean flag = false;
		if(null != this.param){
			if(this.param.containsKey("MsgFltTime") && this.param.containsKey("MsgFltLo") && this.param.containsKey("MsgFltLa") && this.param.containsKey("VehicleID")){
				if(null!= this.param.get("MsgFltTime") && null!= this.param.get("VehicleID"))
					flag = true;
			}
		}
		return flag;
	}
}
