package net.tycmc.bulb.note.core.consumer.fltnote;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.tycmc.bulb.note.core.Util.JsonToMap;
import net.tycmc.bulb.note.core.Util.NoteFile;
import net.tycmc.bulb.note.core.handler.MQConnectionFactory;
import net.tycmc.bulb.note.core.handler.MQenum;
import net.tycmc.bulb.note.core.thread.FltAlarmRunner;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import com.tycmc.functions.util.DynamicReadConfig;


public class FltAlarmConsumer{
	Logger log = Logger.getLogger("FltAlarmConsumer");
	@Autowired
	private MQConnectionFactory connFactory;
	
	
	/**
	 * 定义消费故障报警消息的具体方法
	 **/
	public void fltAlarm(){
		NoteFile noteFile = null; 
		SimpleDateFormat sdf = null;
		JsonToMap jtUtil = new JsonToMap();
		//ExecutorService	executorService = Executors.newFixedThreadPool(15);
		ExecutorService	executorService = Executors.newCachedThreadPool();
		boolean sendFltNote = this.getSentFltNote("sendFltAlarmNote");
		boolean sendFltAPP = this.getSentFltNote("sendFltAlarmAPP");
		boolean sendFltAPPSP = this.getSentFltNote("sendFltAlarmAPPSP");
		boolean sendFltAPPJM = this.getSentFltNote("sendFltAlarmAPPJM");
		boolean writeFlt = this.getSentFltNote("writeFltAlarmNote");
		boolean GcicsendFltAlarmNote=this.getSentFltNote("GcicsendFltAlarmNote");//工程机械版是否发动短信
		boolean sendFltAlarmAPPJMSP = this.getSentFltNote("sendFltAlarmAPPJMSP");//非道路支持板
		boolean sendFltAlarmNoRoadAPPIOS = this.getSentFltNote("sendFltAlarmNoRoadAPPIOS");//道路ios推送
		boolean sendFltAlarmRoadAPPIOS = this.getSentFltNote("sendFltAlarmRoadAPPIOS");//道路ios推送 
		if(writeFlt){
			noteFile = new NoteFile("FltNote");
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
		String apiKey = "";
		String secretKey = "";
		String apiKeySP = "";
		String secretKeySP = "";
		String apiKeyJM="";
		String secretKeyJM = "";
		String apiKeyIOS="";
		String secretKeyIOS="";
		String CT300="";//CT300版本号 用于工程机械版控制
		String Sys_Gcic="";//工程机械版系统id
		String GCICName="";//工程机械版数据库名称
		try {
			apiKey = DynamicReadConfig.getConfigProperty("app.properties", "apiKey");
			secretKey = DynamicReadConfig.getConfigProperty("app.properties", "secretKey");
			apiKeySP = DynamicReadConfig.getConfigProperty("app.properties", "apiKeySP");
			secretKeySP = DynamicReadConfig.getConfigProperty("app.properties", "secretKeySP");
			apiKeyJM = DynamicReadConfig.getConfigProperty("app.properties", "apiKeyJM");
			secretKeyJM = DynamicReadConfig.getConfigProperty("app.properties", "secretKeyJM");
			apiKeyIOS = DynamicReadConfig.getConfigProperty("app.properties", "apiKeyIOS");
			secretKeyIOS = DynamicReadConfig.getConfigProperty("app.properties", "secretKeyIOS");
			CT300=DynamicReadConfig.getConfigProperty("softveragreement.properties", "CT300");
			Sys_Gcic=DynamicReadConfig.getConfigProperty("config.properties", "Sys_Gcic");
			GCICName=DynamicReadConfig.getConfigProperty("config.properties", "GCICNAME");
		} catch (ConfigurationException e1) {
//			Thread.sleep(100000000);
			sendFltAPP = false;
			sendFltAPPSP = false;
			sendFltAPPJM = false;
		}
		try {
			Connection connection = connFactory.getConnection();
			Channel channel = connection.createChannel();
			channel.exchangeDeclare(MQenum.fltExchange.getEnumValue(), MQenum.mqExchangeType.getEnumValue(), true);
			channel.queueDeclare(MQenum.fltQueue.getEnumValue(), true, false, false, null);
			channel.queueBind(MQenum.fltQueue.getEnumValue(), MQenum.fltExchange.getEnumValue(), MQenum.fltRouteKey.getEnumValue());
			QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume(MQenum.fltQueue.getEnumValue(), false,consumer);
			String message = null;
			StringBuffer str = new StringBuffer();
			int i=1;
			while(true){
				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
				message = new String(delivery.getBody(),"UTF-8");
				i++;
				str.append(sdf.format(new Date())+":"+message+" \r\n");
				if(writeFlt && i%1==0&&str.toString().length()>0){
					noteFile.writeOutNote(str.toString());
					str = new StringBuffer();
					i=0;
				}
				Map<String,Object> oneAlarm = jtUtil.jsonToMapSingle(message);
				if(null!=oneAlarm && !oneAlarm.isEmpty()){
					oneAlarm.put("sendNote",String.valueOf(sendFltNote));
					oneAlarm.put("GcicsendNote", String.valueOf(GcicsendFltAlarmNote));
					oneAlarm.put("sendAPP",String.valueOf(sendFltAPP));
					oneAlarm.put("sendAPPSP",String.valueOf(sendFltAPPSP));
					oneAlarm.put("sendAPPJM",String.valueOf(sendFltAPPJM));
					oneAlarm.put("sendFltAlarmAPPJMSP",String.valueOf(sendFltAlarmAPPJMSP));
					oneAlarm.put("sendFltAlarmNoRoadAPPIOS",String.valueOf(sendFltAlarmNoRoadAPPIOS));
					oneAlarm.put("sendFltAlarmRoadAPPIOS",String.valueOf(sendFltAlarmRoadAPPIOS));
					oneAlarm.put("apiKey", apiKey);
					oneAlarm.put("secretKey", secretKey);
					oneAlarm.put("apiKeySP", apiKeySP);
					oneAlarm.put("secretKeySP", secretKeySP);
					oneAlarm.put("apiKeyJM", apiKeyJM);
					oneAlarm.put("secretKeyJM", secretKeyJM);
					oneAlarm.put("apiKeyIOS", apiKeyIOS);
					oneAlarm.put("secretKeyIOS", secretKeyIOS);
					oneAlarm.put("CT300", CT300);
					oneAlarm.put("Sys_Gcic", Sys_Gcic);
					oneAlarm.put("GCICNAME", GCICName);
					
					//将故障报警消息封装为线程，并提交给线程池
					executorService.submit(new FltAlarmRunner(oneAlarm));
				}else{
					log.info("FltAlarmConsumer"+sdf.format(new Date())+"接收到不能解析的消息——"+message);
				}
				
				channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
			}
		} catch (ShutdownSignalException e) {
			e.printStackTrace();
		} catch (ConsumerCancelledException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 获取是否发送故障报警短信
	 **/
	private boolean getSentFltNote(String proType) {
		boolean flag = false;
		try {
			String sendNote = DynamicReadConfig.getConfigProperty("note.properties", proType);
			if(null!=sendNote&&sendNote.equalsIgnoreCase("true"))
				flag = true;
		} catch (ConfigurationException e) {
			
		}
		return flag;
	}
}
