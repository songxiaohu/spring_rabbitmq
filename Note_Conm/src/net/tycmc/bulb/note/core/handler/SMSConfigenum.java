package net.tycmc.bulb.note.core.handler;
/**
 * 短信配置类
 */
public enum SMSConfigenum {
	
	/**
	 * 发送资源号
	 */
	SourceNumber("625570012188"),
	/**
	 * 每天发送短信开始时间默认值
	 */
	SendTime_Bgn("08:00:00"),
	/**
	 * 每天发送短信结束时间默认值
	 */
	SendTime_End("18:00:00"),
	/**
	 * 故障报警发送颜色（红色）
	 */
	FaultCodeColorInfo("#FF0000"),
	/**
	 * 故障代码颜色--红色
	 */
	FltCodeRed("红色"),
	/**
	 * 故障代码颜色--黄色
	 */
	FltCodeYellow("黄色"),
	/**
	 * 电控故障短信
	 */
	SMSCfgFlt("1"),	
	/**
	 * 保养计划提醒短信
	 */
	SMSCfgMtn("2"),
	/**
	 * 日报告
	 */
	SMSCfgDayReport("3"),
	/**
	 * 机械报警
	 */
	SMSCfgAlarm("4"),
	/**
	 * 司机行为
	 */
	SMSCfgDriverTip("5"),
	/**
	 * 载重监控
	 */
	SMSCfgLoad("6"),
	/**
	 * 故障解除
	 */
	SMSCfgFltRels("7"),
	/**
	 * 服务期限
	 */
	SMSCfgService("8"),
	/**
	 * 门控报警
	 */
	SMSCfgGatAlarm("9"),
	/**
	 * 怠速提醒
	 */
	SMSCfgIdling("10"),
	/**
	 * 故障预警
	 */
	SMSCfgFltForward("11"),
	/**
	 * 烟度报警
	 */
	SMSCfgSmoke("12"),
	/**
	 * 烟度解除
	 */
	SMSCfgSmokeRe("13"),
	/**
	 * 温度报警
	 */
	SMSCfgTemp("14"),
	/**
	 * 温度解除
	 */
	SMSCfgTempRe("15"),
	/**
	 * 进出围栏提示
	 */
	SMSCfgIOFen("16"),
	/**
	 * 时效预警
	 */
	SMSCfgRateOfHourForward("17"),
	/**
	 * 4.19新增 液位传感器报警lvyingzhu
	 */
	SMSCfgTankLevel("18");
	private String name;

	public String getName() {
		return name;
	}
	
	SMSConfigenum(String name){
		this.name = name;
	}
	
}
