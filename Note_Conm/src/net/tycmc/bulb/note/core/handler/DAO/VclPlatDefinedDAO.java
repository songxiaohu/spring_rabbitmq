package net.tycmc.bulb.note.core.handler.DAO;

import java.util.Map;

import net.tycmc.bulb.dbutil.global.GlobalDBVar;
import net.tycmc.bulb.note.core.handler.MyBaseDAO;

public class VclPlatDefinedDAO {
	private MyBaseDAO myBaseDAO;
	public VclPlatDefinedDAO(){
		if(null==myBaseDAO){
			this.myBaseDAO = new MyBaseDAO();
		}
	}
	/**
	 * 根据fltcode、fltspn、fltfmi查询平台定义的故障信息
	 * keySet:MsgFltSPN、MsgFltFMI、MsgFltCode
	 **/
	public Map<String, Object> queryFltInfoOnPlatDefined(
			Map<String, Object> param) {
		if(null!=param.get("MsgFltSPN")&&null!=param.get("MsgFltFMI")){
			String sql = "select SysISM_Code as MsgFltCode,SysISM_CodeColorInfo as FltColorInfo,SysISM_ContentCN_Abbr as FltContentAbbr, SysISM_ContentCN as FltContent,SysISM_Risk as FltRisk, "
			          + "SysISM_Code as FltCode,SysISM_SPN as FltSPN,SysISM_FMI as FltFMI,SysISM_Level as FltLevel,SysISM_ContentEN_Abbr as FltContentAbbr_EN,SysISM_ContentEN as FltContent_EN,SysISM_Risk_EN as FltRisk_EN from LINK_CTY_Vcl.Cty_vcl.dbo.Sys_FltISM"
					  + " where 1=1 and  SysISM_SPN = ? and SysISM_FMI = ? ";
			return this.myBaseDAO.getBaseDAO().execQueryOne(sql, new Object[]{param.get("MsgFltSPN"),param.get("MsgFltFMI")});
		}
		return null;
	}
	/**
	 * 查询平台库定义的的报警信息(因为小一设备定义的报警项字典表包含大一的所有项，这里只查询小一库)
	 * 6	线束被拆报警	,	7	电瓶被拆报警	,	11	GPS天线报警	,	12	CAN通信报警	,	13	GSM天线报警		
	 **/
	public Map<String,Object> queryAlarmInfoOnPlatDefined(Map<String, Object> param){
		if(null!= param.get("MsgAlmTypeID")){
			String sql = "select SysA_ItemCN as MsgAlmTypeName from "+GlobalDBVar.getDBFullName_Conn("Sys_Alarm", "3", null)+" where SysA_ID = ? ";
			return this.myBaseDAO.getBaseDAO().execQueryOne(sql, new Object[]{param.get("MsgAlmTypeID").toString()});
		}
		return null;
	}
	
	/**
	 * 根据设备ID查询设备信息
	 **/
	public Map<String,Object> queryVclInfo(String vehicleID,String GCICName){
		return this.myBaseDAO.getBaseDAO().execQueryOne("select VehicleNumber,DBCode,ESNNumber,VehicleContact,VehicleChargedMobile,OEMName,OEMTel,Tmnl_SoftEdition from "+GCICName+"V_VehicleInfo where VehicleID = ?", new Object[]{vehicleID});
	}
	/**
	 * 根据设备ID查询设备信息
	 **/
	public Map<String,Object> queryVclSysTemId(String vehicleID){
		//return this.myBaseDAO.getBaseDAO().execQueryOne("select Tmnl_SysI_ID,Tmnl_SoftEdition from "+GlobalDBVar.getDBFullName_Conn("Tmnl_Info", "1", null)+" inner join "+GlobalDBVar.getDBFullName_Conn("Tmnl_Install_Last", "1", null)+" on TmnlIL_Tmnl_ID=Tmnl_ID where TmnlIL_Vcl_ID = ? ", new Object[]{vehicleID});
		String sql="select Vcl_SysI_ID from LINK_CTY_Vcl.CTY_VCL.DBO.Vcl_Info where Vcl_ID="+vehicleID;
		return this.myBaseDAO.getBaseDAO().execQueryOne(sql, new Object[]{});
	}
}
