package net.tycmc.bulb.note.core.handler;

import net.tycmc.bulb.common.dao.BaseDAO;
import net.tycmc.bulb.common.datasource.ProxoolDataSource;

public class MyBaseDAO {

	private BaseDAO baseDAO;
	
	private static String proxool = "proxool.DateBase";
	
	public  MyBaseDAO() {
		baseDAO = new BaseDAO();
		ProxoolDataSource connGetter = new ProxoolDataSource();
		connGetter.setProxoolname(proxool);
		baseDAO.setConnGetter(connGetter);
	}

	/**
	 * @param baseDao the baseDao to set
	 */
	public void setBaseDAO(BaseDAO baseDAO) {
		this.baseDAO = baseDAO;
	}

	public BaseDAO getBaseDAO() {
		return baseDAO;
	}
}
