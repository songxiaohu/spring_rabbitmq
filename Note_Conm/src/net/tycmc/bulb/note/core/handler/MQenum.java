package net.tycmc.bulb.note.core.handler;
/**
 * 定义rabbitmq的routekey（bindingkey）、exchange名称、queue名称
 **/
public enum MQenum {
	gateRouteKey("gateAlarm"),//门控报警routeKey
	iofenRouteKey("infenAlarm"),//进出围栏报警routeKey
	physicsRouteKey("physicsAlarm"),//机械报警routeKey
	sensorRouteKey("sensorAlarm"),//传感器报警routeKey
	fltRouteKey("fltAlarm"),//故障报警routeKey
	leveRouteKey("tankleveAlarm"),//液位传感器报警routeKey
	accountBehaviorRouteKey("useraccess"),//用户行为分析routeKey
	
	gateExchange("gateAlarmExchange"),//门控报警交换器
	iofenExchange("infenAlarmExchange"),//进出围栏提示交换器
	physicsExchange("physicsAlarmExchange"),//机械报警交换器
	sensorExchange("sensorAlarmExchange"),//传感器报警交换器
	fltExchange("fltAlarmExchange"),//故障报警交换器
	levelExchange("tankleveAlarmExchange"),//液位传感器报警交换器
	accountBehaviorExchange("useraccessExchange"),//用户行为分析交换器
	
	gateQueue("gateAlarmQueue"),//门控报警Queue
	iofenQueue("infenAlarmQueue"),//进出围栏提示Queue
	physicsQueue("physicsAlarmQueue"),//机械报警Queue
	sensorQueue("sensorAlarmQueue"),//传感器报警Queue
	fltQueue("fltAlarmQueue"),//故障报警Queue
	levelQueue("tanklevelAlarmQueue"),//液位传感器报警Queue
	accountBehaviorQueue("useraccessqueue"),//用户行为分析Queue
	mqExchangeType("fanout");//exchange工作模式，目前使用topic
	private String enumValue;
	
	public String getEnumValue() {
		return enumValue;
	}

	public void setEnumValue(String enumValue) {
		this.enumValue = enumValue;
	}

	private MQenum(String enumValue){
		this.enumValue = enumValue;
	}
}
