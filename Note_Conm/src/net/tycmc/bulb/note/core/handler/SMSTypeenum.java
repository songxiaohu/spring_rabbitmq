package net.tycmc.bulb.note.core.handler;
/**
 * (name,type):name用于查询短信接收人，type:用于流程处理，
 * 故障、传感器的name未使用
 **/
public enum SMSTypeenum {
	//type——1：故障、2：报警、3：传感器报警、4门控报警、5：进出围栏提示、6：服务期报警、7：自动安装、8：保养提示
	smsFlt("Flt",1),
	smsAlarm("SMSAS_SMSAlm",2),
	smsSensor("sensor",3),
	smsGate("SMSAS_SMSMkbj",4),
	smsIOFen("SMSAS_SMSFenTip",5),
	smsServerLine("",6),
	smsAutoInstall("",7),
	smsMaint("",8),
	smsTankLevel("SMSAS_SMSTankLevel",9);
	private String smsName;
    private int smsType;
	
	public String getSmsName() {
		return smsName;
	}

	public int getSmsType() {
		return smsType;
	}

	private SMSTypeenum(String  smsName,int smsType){
		this.smsName = smsName;
		this.smsType = smsType;
	}
}
