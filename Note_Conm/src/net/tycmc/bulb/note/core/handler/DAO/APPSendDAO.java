package net.tycmc.bulb.note.core.handler.DAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.apache.commons.collections.map.CaseInsensitiveMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.tycmc.bulb.common.dao.BaseDAO;
import net.tycmc.bulb.common.dao.TransactionDAO;
import net.tycmc.bulb.common.datasource.Connectionable;
import net.tycmc.bulb.common.datasource.ProxoolDataSource;
import net.tycmc.bulb.common.util.MapGetter;
import net.tycmc.bulb.note.core.Util.DateList;
import net.tycmc.bulb.note.core.Util.MsgTobaidyun;
import net.tycmc.bulb.note.core.Util.PushMsgToIOS;
import net.tycmc.bulb.note.core.handler.AppContentCfg;
import net.tycmc.bulb.note.core.handler.MyBaseDAO;
import net.tycmc.bulb.note.core.handler.MyTransactionDAO;

import com.tycmc.functions.util.StringUtil;

public class APPSendDAO {
	private MyBaseDAO myBaseDAO;
	private MyTransactionDAO myTranDAO;
	
	public APPSendDAO(boolean isGcic){
		if(true==isGcic){
			myBaseDAO=new MyBaseDAO();
			BaseDAO baseDAO = new BaseDAO();
			ProxoolDataSource connGetter = new ProxoolDataSource();
			connGetter.setProxoolname("proxool.GcicDateBase");
			baseDAO.setConnGetter(connGetter);
			this.myBaseDAO.setBaseDAO(baseDAO);
			myTranDAO=new MyTransactionDAO();
			TransactionDAO tranDAO = new TransactionDAO();
			ProxoolDataSource connGetterTran = new ProxoolDataSource();
			connGetterTran.setProxoolname("proxool.GcicDateBase");
			tranDAO.setConnGetter((Connectionable)connGetterTran);
			this.myTranDAO.setTranDAO(tranDAO);
		}else{
			myBaseDAO=new MyBaseDAO();
			myTranDAO=new MyTransactionDAO();
		}
	}

	public boolean sendAPP(Map<String, Object> param,int type) {
		boolean flag = false;
		System.out.println("发送类型---------------------"+type);
		//app 接收人
		List<Map<String,Object>> appUsersList= this.getAppUserOfMsgs(param,type);
		List<Map<String,Object>> CloudPushlist= new ArrayList<Map<String,Object>>();
		List<String> sqlList = new ArrayList<String>();
		List<Object[]> sqlparam = new ArrayList<Object[]>();
		
		InsertApp(appUsersList, param, sqlList, sqlparam,CloudPushlist,type);
		SaveAPPNote(appUsersList,param,sqlList, sqlparam,type);
		if(sqlList.size()>0 && sqlparam.size()>0 && sqlList.size() == sqlparam.size()){
			flag = this.myTranDAO.getTranDAO().batch(sqlList, sqlparam);
		}
		String apiKey="";
		String secretKey="";
		//调用百度云，发送App信息给用户
		switch (type) {
		case 1:
			apiKey=MapGetter.getString(param, "apiKey");
			secretKey=MapGetter.getString(param, "secretKey");
			break;
		case 2:
			apiKey=MapGetter.getString(param, "apiKeySP");
			secretKey=MapGetter.getString(param, "secretKeySP");
			break;
		case 3:
			apiKey=MapGetter.getString(param, "apiKeyJM");
			secretKey=MapGetter.getString(param, "secretKeyJM");
			break;
		case 4://非道路 服务班
			apiKey=MapGetter.getString(param, "apiKeyIOS");
			secretKey=MapGetter.getString(param, "secretKeyIOS");
			break;
		case 5://非道路IOS
			break;
		case 6://道路IOS
			break;
		default:
			break;
		}
		if(5==type||6==type){
			new PushMsgToIOS().sendMsg(CloudPushlist,type);
			return flag;
		}
		
		new MsgTobaidyun().sendMsg(CloudPushlist,param,apiKey,secretKey);
		return flag;
	}
	/**
	 * 推送app的信息保存入消息中心
	 **/
	private void SaveAPPNote(List<Map<String, Object>> appUsersList,
			Map<String, Object> smsMap, List<String> sqlList,
			List<Object[]> sqlparam,int type) {
		if(type==5||type==6){
			return;
		}
		String sql = "";
		Object[] param = null;
		String appContent = AppContentCfg.appFlt.replaceAll("X_VclNum", MapGetter.getString(smsMap,"VehicleNumber")).
				replaceAll("X_Date", DateList.getFormatDate(MapGetter.getString(smsMap, "MsgFltTime"), 11)).replaceAll("X_FltCode", MapGetter.getString(smsMap,"FltCode"))
				.replaceAll("X_FltMsgAbbr", MapGetter.getString(smsMap,"FltContentAbbr")).replaceAll("X_Risk", MapGetter.getString(smsMap, "FltRisk"));
		String Fltlevel = MapGetter.getString(smsMap, "FltLevel");
		if(null!=appUsersList && !appUsersList.isEmpty()){
			for(Map<String,Object> user:appUsersList){
				String Level=MapGetter.getString(user,"fltlevel");
//				String vehiclenumber=MapGetter.getString(user,"vehiclenumber");
				if(Fltlevel.equals(Level) && !"".equals(Fltlevel)&& !"null".equals(Fltlevel.toLowerCase()) ){//此条故障应推送给App,所以进行保存新表
						sql = "insert into Note_Flt(NF_VclID,NF_SType,NF_State,NF_MsgContent,NF_Position,NF_UserID,NF_UserName,NF_UserDBCode,NF_UserDBName,NF_Mobile,NF_SendTime)" +
								"values(?,"+(1==type||3==type?"2":"3")+",1,?,?,?,?,?,?,?,GETDATE());";
						param = new Object[]{
								MapGetter.getString(smsMap, "VehicleID"),
								appContent,
								MapGetter.getString(smsMap, "Position"),
								MapGetter.getString(user, "UserID"),
								MapGetter.getString(user, "UserName"),
								MapGetter.getString(user, "UserDBCode").substring(0,MapGetter.getString(user, "UserDBCode").length()-1),
								MapGetter.getString(user, "UserDBName").substring(0,MapGetter.getString(user, "UserDBName").length()-1),
								MapGetter.getString(user, "Mobile")
						};
						sqlList.add(sql);
						sqlparam.add(param);
				}
			}	
		}
	}

	/**
	 * 查询某台设备app的接收人信息
	 **/
	private List<Map<String, Object>> getAppUserOfMsgs(Map<String, Object> param,int type) {
		List<Map<String,Object>> userList = new ArrayList<Map<String,Object>>();
		StringBuilder sql = new StringBuilder("select AS_accountID as AccountID,AS_deployContent,VehicleID,VehicleNumber,vu.UserID,vu.UserName,vu.Mobile,vu.Email,");
		if(1 == type){
			sql.append(" vu.baiduID_APP,vu.bdChnID_APP,vu.applan as lan,token=0,");
		}else if(2==type){
			sql.append(" vu.baiduID_APP_SP baiduID_APP,vu.bdChnID_APP_SP bdChnID_APP,vu.appsplan as lan,token=0,");
		}else if(3==type){
			sql.append(" ma.AAM_baiduID  baiduID_APP,ma.AAM_bdChnID bdChnID_APP,lan=0,token=0,");
		}else if(4==type){//从GCIC库中获取推送的token
			sql.append(" sp.AAS_baiduID  baiduID_APP,sp.AAS_bdChnID bdChnID_APP,sp.AAS_TOKEN as token,lan=0,");
		}else if(5==type){//从CTY_MS库获取推送的token
			sql.append(" sp.AAS_TOKEN as token,lan=0,");
		}else if(6==type){
			sql.append(" vu.app_ios_token as token,vu.applan as lan,");
		}
		sql.append(" (select a.organizationcode+',' from (SELECT userinfo.userid,organization_db.organizationcode");
		sql.append(" FROM userinfo LEFT OUTER JOIN V_UserInfo_Organization ON userinfo.userid = V_UserInfo_Organization .userid");
		sql.append(" LEFT OUTER JOIN organization_db ON V_UserInfo_Organization.organizationid = organization_db.organizationid)a where a.userid=vu.userid   for xml path('')) as UserDBCode");
		sql.append(" ,(select a.organizationname+',' from (SELECT   userinfo.userid,organization_db.organizationname ");
		sql.append(" FROM userinfo LEFT OUTER JOIN V_UserInfo_Organization ON userinfo.userid = V_UserInfo_Organization .userid");
		sql.append(" LEFT OUTER JOIN organization_db ON V_UserInfo_Organization.organizationid = organization_db.organizationid)a where a.userid=vu.userid   for xml path('')) as UserDBName");
		sql.append(" from V_VehicleInfo vi");
		sql.append(" inner join v_userinfoaccount vu on vi.dbcode like vu.OrganizationCode + '%'");
		if(4==type||5==type){
			sql.append(" inner join APP_AccountInfo_SP sp on sp.AAS_AccountID=vu.AccountID ");
			sql.append(" inner join adviceswitch ads on ads.AS_accountID = vu.AccountID where vehicleid=? and as_type=4");
		}else if(3==type){
			sql.append(" inner join app_accountinfo_ma ma on ma.AAM_AccountID=vu.AccountID ");
			sql.append(" inner join adviceswitch ads on ads.AS_accountID = vu.AccountID where vehicleid=? and as_type="+type);
		}else if(6==type){
			//sql.append(" inner join app_accountinfo_ma ma on ma.AAM_AccountID=vu.AccountID ");
			sql.append(" inner join adviceswitch ads on ads.AS_accountID = vu.AccountID where vehicleid=? and as_type=1");
		}else{
			sql.append(" inner join adviceswitch ads on ads.AS_accountID = vu.AccountID where vehicleid=? and as_type="+type);
		}
		
		List<Map<String,Object>> tempList = this.myBaseDAO.getBaseDAO().execQuery(sql.toString(), new Object[]{MapGetter.getString(param,"VehicleID")});
		if(null!=tempList){
			userList = new ArrayList<Map<String,Object>>();
			JSONArray json = null;
			for(Map<String,Object> temp:tempList){
				if(StringUtil.isValid(MapGetter.getString(temp, "AS_deployContent"))){
					json = JSONArray.fromObject(MapGetter.getString(temp, "AS_deployContent"));
					for(int i=0;i<json.size();i++){//遍历
						Map<String,Object> maps = new HashMap<String,Object>();
						
						JSONObject jsonMap = JSONObject.fromObject(json.get(i));
						
						String lv1 = jsonMap.getString("lv1").toString();
						String lv2 = jsonMap.getString("lv2").toString();
						lv2 = this.getFltlevel(lv2,type);
						
						String stat =jsonMap.getString("stat").toString();
						//if(stat.equals("1") && "1".equals(lv1) && null!=lv2 ){
						if(stat.equals("1") && null!=lv2 ){
							maps.put("vehicleid",MapGetter.getString(temp,"vehicleid"));
							maps.put("accountid", MapGetter.getString(temp,"accountid" ));
							maps.put("fltlevel",lv2);
							maps.put("VehicleNumber",MapGetter.getString(temp,"VehicleNumber" ) );
							maps.put("baiduID_APP",MapGetter.getString(temp,"baiduID_APP"));
							maps.put("bdChnID_APP",MapGetter.getString(temp,"bdChnID_APP"));
							maps.put("UserID",MapGetter.getString(temp,"UserID"));
							maps.put("UserName",MapGetter.getString(temp,"UserName"));
							maps.put("Mobile",MapGetter.getString(temp,"Mobile"));
							maps.put("Email",MapGetter.getString(temp,"Email"));
							maps.put("UserDBCode",MapGetter.getString(temp,"UserDBCode"));
							maps.put("UserDBName",MapGetter.getString(temp,"UserDBName"));
							maps.put("lan",MapGetter.getString(temp,"lan"));
							maps.put("token", MapGetter.getString(temp, "token"));
							userList.add(maps);
						}
					}
				}
			}
		}
		return userList;
	}
	/*
	public static void main(String[] args) {
		String s = "AccountID;AS_deployContent;VehicleID;VehicleNumber;baiduID_APP;bdChnID_APP;UserID;UserName;"
				+"@180	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	732392115181502000	;4114516608420540000;	304	;管理员       ;"
				+"@181	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	656497326030922000	;3895542147169190000;	305	;SuperManager ;"
				+"@196	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	 NULL	              ;NULL	            ;   316	;王玉友	      ;"
				+"@197	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	1073746676002640000	;4514050364514680000;	317	;刘思广	      ;"
				+"@202	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	               NULL	;NULL	            ;   322	;QC	          ;"
				+"@203	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	982698452211746000	;4138526391716650000;	323	;kelly	      ;"
				+"@204	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	783650290784714000	;4094324209980920000;	324	;赵天明	      ;"
				+"@212	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	684723444739874000	;3640343212714750000;	332	;李余良	      ;"
				+"@220	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;			            ;                   ;   343	;王波	        ;"
				+"@225	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	743359034087100000	;3657205035210790000;	383	;李晓飞	      ;"
				+"@241	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	587354789177890000	;3678414180301360000;	394	;唐志静	      ;"
				+"@242	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;			                ;                ; 395	;潘雪山	      ;"
				+"@244	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	696300272028086000	;4341875785718670000;	402	;林晓静	      ;"
				+"@245	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;			                ;                   ; 404	;徐宏亮	      ;"
				+"@254	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	               NULL	;              NULL	;418	;李明露	      ;"
				+"@310	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	658731673129981000	;3545007676642760000;	471	;李阳	        ;"
				+"@415	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	741354092308624000	;4248803513141690000;	574	;徐云钊	      ;"
				+"@474	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	908421553969161000	;4070167383558880000;	647	;王全岗	      ;"
				+"@475	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	912140445754156000	;3552883194564000000;	648	;陈迪	        ;"
				+"@488	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;                NULL	;               NULL;	661	;赵文茂3      ;"
				+"@527	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	630665766041563000	;4190714609613670000;	699	;何月涛	      ;"
				+"@579	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;			                ;                   ; 751	;实习生	      ;"
				+"@627	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	741354092308624000	;4248803513141690000;	799	;路银平	      ;"
				+"@665	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	              NULL	;NULL	              ;835	;John-CTY			;"    
				+"@668	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	              NULL	;NULL	              ;838	;John-XCEC		;"     
				+"@669	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"0\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	              NULL	;NULL	              ;839	;JoeMen       ;"
				+"@68	;[{\"lv1\":\"1\",\"lv2\":\"1\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"2\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"3\",\"stat\":\"1\"},{\"lv1\":\"1\",\"lv2\":\"4\",\"stat\":\"0\"}];	127541;	陕120171;	              NULL	;NULL	              ;6  ;超级管理;";
		String[] temp0 = s.split("@");
		String[] title = temp0[0].split(";");
		List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
	    for(int i=1;i<temp0.length;i++){
	    	String[] one = temp0[i].split(";");
	    	Map<String,Object> hash = new CaseInsensitiveMap();
	    	for(int j=0;j<one.length;j++){
	    		if(StringUtil.isValid(title[j])){
	    			hash.put(title[j], one[j]);
	    		}
	    	}
	    	list.add(hash);
	    }
	    List<Map<String,Object>> userList = new ArrayList<Map<String,Object>>();
	    JSONArray json = null;
		for(Map<String,Object> temp:list){
			if(StringUtil.isValid(MapGetter.getString(temp, "AS_deployContent"))){
				json = JSONArray.fromObject(MapGetter.getString(temp, "AS_deployContent"));
				for(int i=0;i<json.size();i++){//遍历
					Map<String,Object> maps = new HashMap<String,Object>();
					
					JSONObject jsonMap = JSONObject.fromObject(json.get(i));
					
					String lv1 = jsonMap.getString("lv1").toString();
					String lv2 = jsonMap.getString("lv2").toString();
					lv2 = getFltlevel(lv2);
					
					String stat =jsonMap.getString("stat").toString();
					if(stat.equals("1") && "1".equals(lv1) && null!=lv2 ){
						maps.put("vehicleid",MapGetter.getString(temp,"vehicleid"));
						maps.put("accountid", MapGetter.getString(temp,"accountid" ));
						maps.put("fltlevel",lv2);
						maps.put("VehicleNumber",MapGetter.getString(temp,"VehicleNumber" ) );
						maps.put("baiduID_APP",MapGetter.getString(temp,"baiduID_APP"));
						maps.put("bdChnID_APP",MapGetter.getString(temp,"bdChnID_APP"));
						maps.put("UserID",MapGetter.getString(temp,"UserID"));
						maps.put("UserName",MapGetter.getString(temp,"UserName"));
						maps.put("Mobile",MapGetter.getString(temp,"Mobile"));
						maps.put("Email",MapGetter.getString(temp,"Email"));
						maps.put("UserDBCode",MapGetter.getString(temp,"UserDBCode"));
						maps.put("UserDBName",MapGetter.getString(temp,"UserDBName"));
						userList.add(maps);
					}
				}
			}
		}
		for(int m=0;m<userList.size();m++){
			Map<String,Object> oneuser = userList.get(m);
			System.out.println(oneuser.get("UserName"));
		}
	    System.out.println(userList.size());
	}
	*/
	public String getFltlevel(String drilevelId,int type){
		int id=Integer.parseInt(drilevelId);
		if(1 == type||6==type){
			switch (id) {
			case 1:
					return "4";
			case 2:
					return "3";
			case 3:
					return "2";
			case 4:
					return "1";
			default:
					return null;
			}
		} else if(2 == type){
			switch (id) {
			case 5:
					return "4";
			case 6:
					return "3";
			case 7:
					return "2";
			case 8:
					return "1";
			default:
					return null;
			}
		}else if(3==type){
			switch (id) {
			case 9:
					return "4";
			case 10:
					return "3";
			case 11:
					return "2";
			case 12:
					return "1";
			default:
					return null;
			}
		
		} else if(4==type||5==type){
			switch (id) {
			case 16:
					return "1";
			case 15:
					return "2";
			case 14:
					return "3";
			case 13:
					return "4";
			default:
					return null;
			}
		
		
		}else{
			return null;
		}

	}

	/**
	 * 向App故障表插入数据
	 **/
	private void InsertApp(List<Map<String,Object>> appUsersList,Map<String,Object> appData,List<String>sqlList,List< Object[]> paramList,List<Map<String,Object>> CloudPushlist ,int type){
		String FltCode = MapGetter.getString(appData, "MsgFltCode");
		String Fltlevel = MapGetter.getString(appData, "FltLevel");
		String VehicleID = MapGetter.getString(appData, "VehicleID"); 
		int resultType=0;
		if(null != appUsersList && !appUsersList.isEmpty()){
			for(Map<String,Object> oneUser:appUsersList){
				String vehiclenumber=MapGetter.getString(oneUser,"VehicleNumber");
				String Level=MapGetter.getString(oneUser,"fltlevel");
				if(Fltlevel.equals(Level)&&!"".equals(Fltlevel) && !"null".equals(Fltlevel.toLowerCase()) ){//此条故障应推送给App
					if(type!=5&&type!=6){//IOS不入库
						String Sql="INSERT INTO APP_FltMsg "+
								"(App_Flt_AccountId "+
								",App_Flt_VclId "+
								" ,App_Flt_FltCode "+
								" ,APP_Flt_fltLevle "+
								",App_Flt_FltMsgTime "+
								" ,App_Flt_fltCont  " +
								" ,App_Flt_fltAllCont " +
								" ,App_Flt_Risk " +
								" ,App_Flt_Spn " +
								" ,App_Flt_Fmi,App_Flt_type,App_Flt_fltCont_EN,App_Flt_fltAllCont_EN,App_Flt_Risk_EN,App_Flt_Count) " +
								" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
						sqlList.add(Sql);
						paramList.add(new Object[]{MapGetter.getString(oneUser,"accountid"),
								VehicleID,FltCode,Fltlevel,MapGetter.getString(appData, "MsgFltTime"),
								MapGetter.getString(appData,"FltContentAbbr"),
								MapGetter.getString(appData,"FltContent"),
								MapGetter.getString(appData,"FltRisk"),
								MapGetter.getString(appData,"FltSPN"),
								MapGetter.getString(appData,"FltFMI"),
								type,
								MapGetter.getString(appData,"FltContentAbbr_EN"),
								MapGetter.getString(appData,"FltContent_EN"),
								MapGetter.getString(appData,"FltRisk_EN"),
								MapGetter.getString(appData,"MsgFltCont")
						});	
					}
					//将信息另存储，用于百度云推送
					Map<String,Object> map=new HashMap<String,Object>();
					if(type!=5&&type!=6){//判断不是ios推送
						if(StringUtil.isValid(MapGetter.getString(oneUser, "baiduID_APP")) && StringUtil.isValid(MapGetter.getString(oneUser, "bdChnID_APP"))){
							map.put("userid", MapGetter.getString(oneUser, "baiduID_APP"));//百度用户id
							map.put("chanlid", MapGetter.getString(oneUser, "bdChnID_APP"));//频道id
							map.put("risk", MapGetter.getString(appData,"FltRisk"));//风险信息
							map.put("vehicleid", VehicleID);//设备id
							map.put("level",Fltlevel);//故障级别
							map.put("fltcode",FltCode);//故障code码
							map.put("fltfmi",MapGetter.getString(appData,"FltFMI") );//故障fmi
							map.put("fltspn", MapGetter.getString(appData,"FltSPN"));//故障spn码
							map.put("fltdetialmsg",MapGetter.getString(appData,"FltContent") );//
							map.put("fltdetialmsg_EN",MapGetter.getString(appData,"FltContent_EN"));
							map.put("vehiclenumber", vehiclenumber);
							map.put("msgtime", MapGetter.getString(appData, "MsgFltTime"));
							map.put("lan", MapGetter.getString(oneUser, "lan"));
							map.put("token", MapGetter.getString(oneUser, "token"));
							CloudPushlist.add(map);	
						}
					}else{
						if(StringUtil.isValid(MapGetter.getString(oneUser, "token"))){
							map.put("risk", MapGetter.getString(appData,"FltRisk"));//风险信息
							map.put("vehicleid", VehicleID);//设备id
							map.put("level",Fltlevel);//故障级别
							map.put("fltcode",FltCode);//故障code码
							map.put("fltfmi",MapGetter.getString(appData,"FltFMI") );//故障fmi
							map.put("fltspn", MapGetter.getString(appData,"FltSPN"));//故障spn码
							map.put("fltdetialmsg",MapGetter.getString(appData,"FltContent") );//
							map.put("fltdetialmsg_EN",MapGetter.getString(appData,"FltContent_EN"));
							map.put("vehiclenumber", vehiclenumber);
							map.put("msgtime", MapGetter.getString(appData, "MsgFltTime"));
							map.put("lan", MapGetter.getString(oneUser, "lan"));
							map.put("token", MapGetter.getString(oneUser, "token"));
							CloudPushlist.add(map);	
						}
					}
					
				}
			}
		}
	}
}

