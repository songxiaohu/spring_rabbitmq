/**
 * rabbitmq 链接工厂
 **/
package net.tycmc.bulb.note.core.handler;

import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class MQConnectionFactory {
	private String mqproFile;
	private volatile ConnectionFactory factory;
	public MQConnectionFactory(){
		if(null==factory){
			factory = new ConnectionFactory();
			if(!(null!=mqproFile&&mqproFile.length()>0)){
				mqproFile = "rabbitmq.properties";
			}
			try {
				PropertiesConfiguration config = new PropertiesConfiguration(mqproFile);
				factory.setHost(config.getString("serverIP"));
				factory.setPort(Integer.parseInt(config.getString("serverPort")));
				factory.setUsername(config.getString("userName"));
				factory.setPassword(config.getString("passWord"));
			} catch (ConfigurationException e) {
				factory.setHost("localhost");
			}
		}
	}
	public Connection getConnection() throws IOException{
	    return factory.newConnection();     	
	}
}
