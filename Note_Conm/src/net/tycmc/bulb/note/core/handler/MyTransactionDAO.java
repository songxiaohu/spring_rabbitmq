package net.tycmc.bulb.note.core.handler;

import net.tycmc.bulb.common.dao.TransactionDAO;
import net.tycmc.bulb.common.datasource.Connectionable;
import net.tycmc.bulb.common.datasource.ProxoolDataSource;

public class MyTransactionDAO {

	private TransactionDAO tranDAO;
	
	private static String proxool = "proxool.DateBase";
	
	public TransactionDAO getTranDAO() {
		return tranDAO;
	}

	/**
	 * @param tranDAO the tranDAO to set
	 */
	public void setTranDAO(TransactionDAO tranDAO) {
		this.tranDAO = tranDAO;
	}

	public  MyTransactionDAO() {
		tranDAO = new TransactionDAO();
		ProxoolDataSource connGetter = new ProxoolDataSource();
		connGetter.setProxoolname(proxool);
		tranDAO.setConnGetter((Connectionable)connGetter);
	}
	
}
