package net.tycmc.bulb.note.core.handler.DAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.tycmc.bulb.common.util.DateUtil;
import net.tycmc.bulb.common.util.MapGetter;
import net.tycmc.bulb.common.util.StringUtil;
import net.tycmc.bulb.note.core.Util.DateRegUtil;
import net.tycmc.bulb.note.core.Util.NoteIndexUtil;
import net.tycmc.bulb.note.core.handler.MyBaseDAO;
import net.tycmc.bulb.note.core.handler.NoteTypeenum;
import net.tycmc.bulb.note.core.handler.SMSTypeenum;

public class SMSLastDAO {
	private MyBaseDAO myBaseDAO;
	
	public SMSLastDAO(){
		if(null==myBaseDAO){
			this.myBaseDAO = new MyBaseDAO();
		}
	} 
	
	
	/**
	 * @return the myBaseDAO
	 */
	public MyBaseDAO getMyBaseDAO() {
		return myBaseDAO;
	}


	/**
	 * @param myBaseDAO the myBaseDAO to set
	 */
	public void setMyBaseDAO(MyBaseDAO myBaseDAO) {
		this.myBaseDAO = myBaseDAO;
	}


	/**
	 * 校验半小时内是否已经发送过短信
	 * 发送过返回true(目前只有故障、机械报警、传感器报警有)
	 **/
	public Map<String,Object> checkSendSMSOfHalfHours(Map<String, Object> param) {
		Map<String,Object> reMap = new HashMap<String,Object>();
		boolean flag = true;
		String  smsType = param.get("smsType").toString();
		//故障、机械报警 、传感器报警才需要进行半小时的逻辑
		if(smsType.equals(String.valueOf(SMSTypeenum.smsFlt.getSmsType())) || smsType.equals(String.valueOf(SMSTypeenum.smsAlarm.getSmsType())) || smsType.equals(String.valueOf(SMSTypeenum.smsSensor.getSmsType())) || smsType.equals(String.valueOf(SMSTypeenum.smsTankLevel.getSmsType()))){
			Map<String,Object> map = null;
			Object[] sqlparam = null;
			NoteIndexUtil noteUtil = new NoteIndexUtil();
			//发送短信的类型
			int noteType = StringUtil.isValid(MapGetter.getString(param,"NoteType"))?Integer.parseInt(MapGetter.getString(param,"NoteType")):0;
			String tablename = noteUtil.getNoteCheckTableName(noteType);
			String tableshort = noteUtil.getNoteCheckTableShort(noteType); 
			String sql = "select top 1 @_ID as id,@_SaveTime as savetime,invalue = Case when (@_SaveTime >= DATEADD(mi,-30,GETDATE()) and @_SaveTime <= GETDATE()) then 1 else 0 end from  #table# where @_VclID = ? ";
			
			if(noteType == NoteTypeenum.noteFlt.getNoteType() || noteType == NoteTypeenum.noteFltRelease.getNoteType() ||noteType == NoteTypeenum.noteFltServe.getNoteType() ||noteType == NoteTypeenum.noteFltServeRe.getNoteType()){
				sql += "and @_SPN = ? and @_FMI = ? order by  @_SaveTime desc";
				sql = sql.replaceAll("#table#", tablename).replaceAll("@", tableshort);
				sqlparam = new Object[]{MapGetter.getString(param, "VehicleID"),
					MapGetter.getString(param, "MsgFltSPN"),
					MapGetter.getString(param, "MsgFltFMI")};
			}else if(noteType == NoteTypeenum.noteAlarm.getNoteType() || noteType == NoteTypeenum.noteAlarmRelease.getNoteType() || noteType == NoteTypeenum.noteSensor.getNoteType() || noteType == NoteTypeenum.noteSensorRelease.getNoteType() || noteType == NoteTypeenum.noteTankLevel.getNoteType()){
				sql += "and @_SysA_ID = ? order by  @_SaveTime desc";
				sql = sql.replaceAll("#table#", tablename).replaceAll("@", tableshort);
				sqlparam = new Object[]{MapGetter.getString(param, "VehicleID"),
					MapGetter.getString(param, "MsgAlmTypeID")};
			}else{
				
			}
			map = this.myBaseDAO.getBaseDAO().execQueryOne(sql.toString(), sqlparam);
			if(null != map && !map.isEmpty()){
				if(MapGetter.getString(map,"invalue").equals("1")){
					flag = false;
				}else{//超过半小时的提醒
					reMap.put("param",map);
				}
			}else{
				
			}
		}
		reMap.put("flag", flag);
		return reMap;
	}
	
	/**
	 * 校验下个工作日
	 * 发送过返回true(故障、机械报警、传感器报警使用到该方法)
	 **/
	public Map<String,Object> checkSendSMSOfNextWorkDay(Map<String, Object> param,List<String> sendTime) {
		boolean flag = true;
		Map<String,Object> reMap = new HashMap<String,Object>();
		String  smsType = param.get("smsType").toString();
		//故障、机械报警 、传感器报警才需要进行校验
		if(smsType.equals(String.valueOf(SMSTypeenum.smsFlt.getSmsType())) || smsType.equals(String.valueOf(SMSTypeenum.smsAlarm.getSmsType())) || smsType.equals(String.valueOf(SMSTypeenum.smsSensor.getSmsType())) || smsType.equals(String.valueOf(SMSTypeenum.smsTankLevel.getSmsType()))){
			String time = "";
			//发送短信的类型
			int noteType = StringUtil.isValid(MapGetter.getString(param,"NoteType"))?Integer.parseInt(MapGetter.getString(param,"NoteType")):0;
			if(noteType == NoteTypeenum.noteFlt.getNoteType() || noteType == NoteTypeenum.noteFltRelease.getNoteType() ||noteType == NoteTypeenum.noteFltServe.getNoteType() ||noteType == NoteTypeenum.noteFltServeRe.getNoteType()){
				time = MapGetter.getString(param,"MsgFltTime");
			}else{
				time = MapGetter.getString(param,"MsgAlmTime");
			}
			time = time.substring(11, time.length());
			
			if(DateRegUtil.compare(time,sendTime.get(0),"HH:mm:ss")&&DateRegUtil.compare(sendTime.get(1),time,"HH:mm:ss")){
				//在工作时间内直接返回
			}else{
				Map<String,Object> map = null;
				Object[] sqlparam = null;
				NoteIndexUtil noteUtil = new NoteIndexUtil();
				String tablename = noteUtil.getNoteCheckTableName(noteType);
				String tableshort = noteUtil.getNoteCheckTableShort(noteType); 
				String sql = "select @_ID as id,@_SendTime as sendtime from  #table# where @_VclID = ?  ";
				
				if(noteType == NoteTypeenum.noteFlt.getNoteType() || noteType == NoteTypeenum.noteFltRelease.getNoteType() ||noteType == NoteTypeenum.noteFltServe.getNoteType() ||noteType == NoteTypeenum.noteFltServeRe.getNoteType()){
					sql += "and @_SPN = ? and @_FMI = ? order by @_SendTime desc";
					sql = sql.replaceAll("#table#", tablename).replaceAll("@", tableshort);
					sqlparam = new Object[]{MapGetter.getString(param, "VehicleID"),
						MapGetter.getString(param, "MsgFltSPN"),
						MapGetter.getString(param, "MsgFltFMI")};
				}else if(noteType == NoteTypeenum.noteAlarm.getNoteType() || noteType == NoteTypeenum.noteAlarmRelease.getNoteType() || noteType == NoteTypeenum.noteSensor.getNoteType() || noteType == NoteTypeenum.noteSensorRelease.getNoteType() || noteType== NoteTypeenum.noteTankLevel.getNoteType()){
					sql += "and @_SysA_ID = ? order by @_SendTime desc";
					sql = sql.replaceAll("#table#", tablename).replaceAll("@", tableshort);
					sqlparam = new Object[]{MapGetter.getString(param, "VehicleID"),
						MapGetter.getString(param, "MsgAlmTypeID")};
				}else{
					
				}
				map = this.myBaseDAO.getBaseDAO().execQueryOne(sql.toString(), sqlparam);
				if(null != map && !map.isEmpty() ){
					String temptime = MapGetter.getString(map,"sendtime");
					//如果已经存在了
					if(DateRegUtil.compare(temptime,DateUtil.addDay(1)+" "+sendTime.get(0),"yyyy-MM-dd HH:mm:ss")){
						flag = false;
					}else{
						reMap.put("param", map);
					}
				}	
			}
		}
		reMap.put("flag", flag);
		return reMap;
	}
	/**
	 **/
	public boolean  updateLastSMSInfo(Map<String, Object> map, int noteType) {
		String updatesql = "update #table# set @_SaveTime = ?,@_SendTime="+(MapGetter.getString(map,"sendtime").equals("null")?"GETDATE()":MapGetter.getString(map,"sendtime"))+"  where @_ID = ? ";
		NoteIndexUtil noteUtil = new NoteIndexUtil();
		String tablename = noteUtil.getNoteCheckTableName(noteType);
		String tableshort = noteUtil.getNoteCheckTableShort(noteType);
		updatesql = updatesql.replaceAll("#table#", tablename).replaceAll("@", tableshort);
		this.myBaseDAO.getBaseDAO().execUpdate(updatesql, new Object[]{MapGetter.getString(map,"msgtime"),MapGetter.getString(map, "id")});
		return true;
	}
	/**
	 **/
	public void saveLastSMSInfo(Map<String, Object> smsMap, int noteType) {
		String savesql = "";
		if(noteType == NoteTypeenum.noteFlt.getNoteType() || noteType == NoteTypeenum.noteFltRelease.getNoteType() ||noteType == NoteTypeenum.noteFltServe.getNoteType() ||noteType == NoteTypeenum.noteFltServeRe.getNoteType()){
			savesql = "insert into Send_Flt_Last(SFL_VclID,SFL_FltCode,SFL_SPN,SFL_FMI,SFL_SaveTime,SFL_SendTime)values(?,?,?,?,?,"+(MapGetter.getString(smsMap, "SendTime").equals("null")?"GETDATE()":MapGetter.getString(smsMap, "SendTime"))+")";				
			this.myBaseDAO.getBaseDAO().execInsert(savesql, 
					new Object[]{MapGetter.getString(smsMap,"VehicleID"),
					MapGetter.getString(smsMap, "MsgFltCode"),
					MapGetter.getString(smsMap, "MsgFltSPN"),
					MapGetter.getString(smsMap, "MsgFltFMI"),
					MapGetter.getString(smsMap, "MsgFltTime")
			});
		}else if(noteType == NoteTypeenum.noteTankLevel.getNoteType() || noteType == NoteTypeenum.noteAlarm.getNoteType() || noteType == NoteTypeenum.noteAlarmRelease.getNoteType() || noteType == NoteTypeenum.noteSensor.getNoteType() || noteType == NoteTypeenum.noteSensorRelease.getNoteType()){
			savesql = "insert into Send_Alarm_Last(SAL_VclID,SAL_SysA_ID,SAL_SaveTime,SAL_SendTime)values(?,?,?,"+(MapGetter.getString(smsMap, "SendTime").equals("null")?"GETDATE()":MapGetter.getString(smsMap, "SendTime"))+")";
			this.myBaseDAO.getBaseDAO().execInsert(savesql, 
					new Object[]{MapGetter.getString(smsMap,"VehicleID"),
					MapGetter.getString(smsMap, "MsgAlmTypeID"),
					MapGetter.getString(smsMap, "MsgAlmTime")
			});
		}
	}
}
