package net.tycmc.bulb.note.core.handler;

/**
 * NoteTypeenum(String tablename, int noteType)
 * tablename_短信保存表，noteType_拼接短信内容时使用
 * 
 **/
public enum NoteTypeenum {
	//1：故障、2：解除故障、3：故障短信（服务）、4：解除故障（服务）、5：故障12小时报警、6：报警、7：解除报警、8：服务期报警、9：自动安装、10：门控报警、11：传感器报警、12：解除传感器报警、13：进出围栏提示、14：保养提示
	noteFlt("Note_Flt",1),
//	noteFltRelease("Note_Flt_Release",2),
	noteFltRelease("Note_Flt",2),
	noteFltServe("Note_Flt_Serve",3),
//	noteFltServeRe("Note_Flt_Release_Serve",4),
	noteFltServeRe("Note_Flt_Serve",4),
	noteFltTwelve("Note_Flt_Twelve",5),
	noteAlarm("Note_Alarm",6),
//	noteAlarmRelease("Note_Alarm_Release",7),
	noteAlarmRelease("Note_Alarm",7),
	noteServerLine("Note_ServerDeadLine",8),
	noteAutoInstallVcl("Note_AutoInstallVcl",9),
	noteGate("Note_GateAlarm",10),
	noteSensor("Note_SensorAlarm",11),
//	noteSensorRelease("Note_SensorAlarm_Release",12),
	noteSensorRelease("Note_SensorAlarm",12),
	noteIOFen("Note_IOFenAlarm",13),
	noteMaint("Note_MaintAlerts",14),
	noteTankLevel("Note_TankLevel",15);
    private int noteType;
    private String tableName;
	public int getNoteType() {
		return noteType;
	}
	public String getTableName() {
		return tableName;
	}
	private NoteTypeenum(String tableName,int noteType){
		this.tableName = tableName;
		this.noteType = noteType;
	}
}
