package net.tycmc.bulb.note.core.handler;

/**
 * 短信内容配置
 * @author chengshichao
 * 创建时间：2012-12-11
 */
public class SMSContentCfg {
	
	public static String X_VclNum = "X_VclNum";
	
	/**
	 * LXF
	 * 格式应为XX月XX日
	 */
	public static String X_Date = "X_Date";
	
	public static String X_FltCode = "X_FltCode";
	
	public static String X_Content = "X_Content";
	
	public static String X_UserName = "X_UserName";
	
	public static String X_WorkHour = "X_WorkHour";
	
	public static String X_WorkKm = "X_WorkKm";
	
	public static String X_Level = "X_Level";
	
	public static String X_DriverTip = "X_DriverTip";
	
	public static String X_BgLoadStatus = "X_BgLoadStatus";
	
	public static String X_EndLoadStatus = "X_EndLoadStatus";
	
	/**
	 * 故障内容
	 */
	public static String SMSCCfgFlt = "X_VclNum-ESNX_ESNN，X_Date，【X_ColorX_FltCode】X_FltMsgAbbr，X_Position，X_UserX_Tel";
	/**
	 * 故障12小时内容
	 */
	public static String SMSCCfgFltTw = "X_VclNum-ESNX_ESNN，X_Date，【X_ColorX_FltCodeX_Tw】X_FltMsgAbbr，X_Position，X_UserX_Tel";
	/**
	 * 故障解除内容
	 */
	public static String SMSCCfgFltRels = "X_VclNum 故障X_FltCode解除，解除时间为X_Date。";
	
	/**
	 * 保养内容（小时）
	 */
	public static String SMSCCfgMtnHour = "尊敬的X_UserName您好，您的X_VclNum预计还有X_WorkHour小时后需要进行X_Level级保养，请知晓。";
	
	/**
	 * 保养内容（公里）
	 */
	public static String SMSCCfgMtnKm = "尊敬的X_UserName您好，您的X_VclNum预计还有X_WorkKm Km后需要进行X_Level级保养，请知晓。";
	
	/**
	 * 报警内容
	 */
	public static String SMSCCfgAlarm = "X_VclNum在X_Date检测到报警，内容：X_Content";
	
	/**
	 * 司机行为内容
	 */
	public static String SMSCCfgDriverTip = "X_VclNum，X_DriverTip，X_Content";
	
	/**
	 * 载重内容
	 */
	public static String SMSCCfgLoad = "X_VclNum在X_Date检测到载重状态变化为：X_BgLoadStatus→X_EndLoadStatus";
	
	/**
	 * 2013-05-09	LXF		add
	 * 服务期限提醒内容
	 */
	public static String SMSCCfgServiceDeadline = "X_VclNum服务期限为X_Date";
	/**
	 * 温度报警
	 */
	public static String SMSCCfgServiceTemp = "X_VclNum在X_Time检测到温度报警，温度值为X_temp摄氏度，请及时处理。";
	/**
	 * 温度报警解除
	 */
	public static String SMSCCfgServiceSMTempRem = "X_VclNum在X_Time解除温度报警，温度值为X_temp摄氏度，请知晓。";
	/**
	 * 烟度报警
	 */
	public static String SMSCCfgServiceSmoke = "X_VclNum在X_Time检测到烟度异常报警，请及时处理。";
	/**
	 * 烟度报警解除
	 */
	public static String SMSCCfgServiceSmokeRem = "X_VclNum在X_Time解除烟度报警，请知晓。";
	/**
	 *进围栏提示(短信)
	 */
	public static String SMSCCfgServiceInFenTip= "X_VclNum在X_Time进入X_Fen围栏，请知晓。";
	/**
	 *出围栏提示(短信)
	 */
	public static String SMSCCfgServiceOutFenTip = "X_VclNum在X_Time离开X_Fen围栏，请知晓。";
	/**
	 *进围栏提示(提示中心)
	 */
	public static String SMSCCfgServiceInFenTipPro= "X_VclNum，进入#X_Fen#围栏。";
	/**
	 *出围栏提示(提示中心)
	 */
	public static String SMSCCfgServiceOutFenTipPro = "X_VclNum，从#X_Fen#围栏离开。";		
	/**
	 *低速报警(短信)
	 */
	public static String SMSCCfgServiceLowSMS = "X_VclNum车速低于X_Speed Km/h已超过X_Mmm分钟，位置：XXXX，请注意。";
	/**
	 *关机报警(短信)
	 */
	public static String SMSCCfgServiceCloseSMS = "X_VclNum本次关机时长超过X_Mmm分钟，位置：XXXX，请注意。";
	/**
	 *开机报警(短信)
	 */
	public static String SMSCCfgServiceOpenSMS = "X_VclNum已开机，本次关机时长X_Mmm分钟，位置：XXXX，请注意。";
	/**
	 *低速报警(提示中心)
	 */
	public static String SMSCCfgServiceLowPro = "X_VclNum，车速低于#X_Speed Km/h#已超过#X_Mmm分钟#";
	/**
	 *关机报警(提示中心)
	 */
	public static String SMSCCfgServiceColsePro = "X_VclNum，本次关机时长超过#X_Mmm分钟#";
	/**
	 *故障提示---模板一
	 */
	public static String SMSCCfgFlt1 = "X_VclNum-ESNX_ESNN，X_Date【X_ColorX_FltCode】X_FltMsgAbbr，X_Risk，服务人X_SerNameX_SerTel，康明斯4008105252（X_VclNum）";
	/**
	 *故障提示---模板二
	 */
	public static String SMSCCfgFlt2 = "X_VclNum-ESNX_ESNN，X_Date【X_ColorX_FltCode】X_FltMsgAbbr，X_Risk，X_FactoryX_OEMTel，康明斯4008105252（X_VclNum）";
	/**
	 *故障提示---模板三
	 */
	public static String SMSCCfgFlt3 = "X_VclNum-ESNX_ESNN，X_Date【X_ColorX_FltCode】X_FltMsgAbbr，X_Risk，X_Position，X_SerNameX_SerTel（X_ESNN）";
	/**
	 *门控报警
	 */
	public static String SMSCCfgGate = "X_VclNum在X_Date检测到门控异常报警，请注意";
	/**
	 *进出围栏提示
	 */
	public static String SMSCCfgIOFen = "X_VclNum在X_Date X_content，请知晓";
	/**
	 * 4.19新增液位传感器报警提示lvyingzhu
	 */
	public static String SMSCCfgTankLevel1 = "X_VclNum在X_Date 检测到油量异常减少，请注意。X_SerNameX_SerTel";
	public static String SMSCCfgTankLevel2 = "X_VclNum在X_Date 检测到油量异常减少，请注意";
	
}
